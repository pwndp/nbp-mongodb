﻿using Anketa1389.Controls;
using Anketa1389.Forms;
using Anketa1389.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anketa1389
{
    public partial class FormMain : Form
    {
        public MongoDBDataLayer client;
        private List<Anketa> _ankete;
        public List<Anketa> Ankete
        {
            get
            {
                return _ankete;
            }
            set
            {
                _ankete = value;
                initData();
            }
        }
        int page, maxPage;

        public FormMain()
        {
            InitializeComponent();
            client = new MongoDBDataLayer();
            page = maxPage = 0;
            List<Anketa> lst = new List<Anketa>();
            Ankete = lst;
            acSlot1.roditelj = this;
            acSlot2.roditelj = this;
            acSlot3.roditelj = this;
        }

        private void initData()
        {
            if (Ankete == null)
                return;

            page = 0;
            maxPage = (Ankete.Count - 1) / 3;
            if (maxPage < 0)
                maxPage = 0;

            btnPrev.Enabled = false;
            if (maxPage == 0)
                btnNext.Enabled = false;
            else
                btnNext.Enabled = true;
        
            refreshSlots();
        }

        public void refreshSlots()
        {
            AnketaControl[] niz = { acSlot1, acSlot2, acSlot3 };
            for (int i = 0; i < 3; i++)
            {
                int poz = page * 3 + i;
                niz[i].Anketa = null;
                if (poz < Ankete.Count)
                    niz[i].Anketa = Ankete[poz];
                niz[i].Prikaz();
            }
        }

        #region Events

        private void btnPrijava_Click(object sender, EventArgs e)
        {
            FormPrijava frm = new FormPrijava();
            frm.client = client;
            frm.ShowDialog();
            Ankete = client.VratiAnkete();
        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbxOznake.Text))
            {
                // Ako je prazan string, samo resetuj
                Ankete = client.VratiAnkete();
                return;
            }

            Ankete = client.VratiAnketeFiltrirano(tbxOznake.Text);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            page--;
            if (page < 0)
                page = 0;
            if (page == 0)
            {
                btnPrev.Enabled = false;
                if (maxPage == 0)
                    btnNext.Enabled = false;
                else
                    btnNext.Enabled = true;
            }
            refreshSlots();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            page++;
            if (page > maxPage)
                page = maxPage;
            if (page == maxPage)
            {
                btnNext.Enabled = false;
                if (maxPage == 0)
                    btnPrev.Enabled = false;
                else
                    btnPrev.Enabled = true;
            }
            refreshSlots();
        }

        private void tbxOznake_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsLetterOrDigit(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar) || Char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
                return;
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            Ankete = client.VratiAnkete();
        }

        #endregion
    }
}

