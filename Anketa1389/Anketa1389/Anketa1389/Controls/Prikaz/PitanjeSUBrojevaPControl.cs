﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Prikaz
{
    public partial class PitanjeSUBrojevaPControl : UserControl
    {
        public PitanjeSUBrojeva pitanje;
        public Form roditelj; 

        public PitanjeSUBrojevaPControl()
        {
            InitializeComponent();
        }

        private void InicijalizujKomponente()
        {
            if ((lblSUBrojevaP == null) || (txtSUBrojevaP == null))
                return;
            lblSUBrojevaP.Text = pitanje.tekstPitanja;
            txtSUBrojevaP.Text = "";

        }

        private void PitanjeSUBrojevaPControl_Load(object sender, EventArgs e)
        {
            InicijalizujKomponente();
            txtSUBrojevaP.Text = pitanje.odgovor.ToString();
        }

        private void txtSUBrojevaP_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtSUBrojevaP.Text))
            { return; }

            var pom = roditelj as FormPopuniAnketu;

            for (int i = 0; i < pom.anketa.listaPitanja.Count; i++)
            {
                if (pom.anketa.listaPitanja[i].tekstPitanja == lblSUBrojevaP.Text && pom.anketa.listaPitanja[i].tip == tipPitanja.slobodanUnosBrojeva)
                {
                    (pom.anketa.listaPitanja[i] as PitanjeSUBrojeva).odgovor = Int32.Parse(txtSUBrojevaP.Text);
                    pitanje.odgovor = Int32.Parse(txtSUBrojevaP.Text);
                }
            }
        }

        private void txtSUBrojevaP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;

                if (String.IsNullOrWhiteSpace(txtSUBrojevaP.Text))
                { return; }

                var pom = roditelj as FormPopuniAnketu;

                for (int i = 0; i < pom.anketa.listaPitanja.Count; i++)
                {
                    if (pom.anketa.listaPitanja[i].tekstPitanja == lblSUBrojevaP.Text && pom.anketa.listaPitanja[i].tip == tipPitanja.slobodanUnosBrojeva)
                    {
                        (pom.anketa.listaPitanja[i] as PitanjeSUBrojeva).odgovor = Int32.Parse(txtSUBrojevaP.Text);
                    }
                }

            }
        }
    }
}
