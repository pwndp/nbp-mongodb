﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Prikaz
{
    public partial class PopunjivanjeAnketaPControl : UserControl
    {
        private Anketa _anketa;
        public Anketa Anketa
        {
            get
            {
                return _anketa;
            }
            set
            {
                _anketa = value;
                initData();
            }

        }
        private readonly string fajl = "popunjeneAnkete.txt";
        public Form Roditelj;

        public PopunjivanjeAnketaPControl()
        {
            InitializeComponent();
        }

        private void initData()
        {
            if (Anketa == null)
                return;

            lblNaziv.Text = Anketa.naziv;
            lblOpis.Text = Anketa.opis;

        }

        private void btnPopuni_Click(object sender, EventArgs e)
        {
            FormPopuniAnketu frm = new FormPopuniAnketu();
            frm.anketa = Anketa;
            frm.client = (Roditelj as FormMain).client;
            DialogResult dr = frm.ShowDialog();
        }
    }
}
