﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Prikaz
{
    public partial class ListaStringPControl : UserControl
    {
        public PitanjeListaStringova pitanje;
        public Form roditelj;
        public bool daLiUpisujemoVrednost = false;

        public ListaStringPControl()
        {
            InitializeComponent();
        }

        private void InicijalizujKomponente()
        {
            if ((lblPitanjeListaStringP == null) || (cbxPitanjeListaStringP == null))
                return;

            lblPitanjeListaStringP.Text = pitanje.tekstPitanja;
            cbxPitanjeListaStringP.DataSource = pitanje.ponudjeniOdgovoriString.ToArray();
            cbxPitanjeListaStringP.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ListaStringPControl_Load(object sender, EventArgs e)
        {
            InicijalizujKomponente();
            OsveziKomponentu();
        }

        private void cbxPitanjeListaStringP_SelectedIndexChanged(object sender, EventArgs e)
        {
            PostavljanjeVrednostiCBX();
        }

        private void PostavljanjeVrednostiCBX()
        {
            var pom = roditelj as FormPopuniAnketu;
            for (int i = 0; i < pom.anketa.listaPitanja.Count; i++)
            {
                if (pom.anketa.listaPitanja[i].tekstPitanja == lblPitanjeListaStringP.Text && pom.anketa.listaPitanja[i].tip == tipPitanja.listaStringova)
                {
                    if (daLiUpisujemoVrednost)
                    {
                        (pom.anketa.listaPitanja[i] as PitanjeListaStringova).odgovor = cbxPitanjeListaStringP.SelectedValue.ToString();
                        daLiUpisujemoVrednost = false;
                    }

                }
            }
        }

        private void OsveziKomponentu()
        {
            if (pitanje != null)
            {
                for (int i = 0; i < cbxPitanjeListaStringP.Items.Count; i++)
                {
                    if (cbxPitanjeListaStringP.GetItemText(cbxPitanjeListaStringP.Items[i]) == pitanje.odgovor)
                    {
                        cbxPitanjeListaStringP.SelectedIndex = i;
                    }
                }
            }
        }

        private void cbxPitanjeListaStringP_SelectionChangeCommitted(object sender, EventArgs e)
        {
            daLiUpisujemoVrednost = true;
            PostavljanjeVrednostiCBX();
        }
    }
}
