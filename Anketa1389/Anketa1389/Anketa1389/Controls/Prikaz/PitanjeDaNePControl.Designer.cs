﻿namespace Anketa1389.Controls.Prikaz
{
    partial class PitanjeDaNePControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDaNeP = new System.Windows.Forms.Label();
            this.rbtDaNePDa = new System.Windows.Forms.RadioButton();
            this.rbtDaNePNe = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // lblDaNeP
            // 
            this.lblDaNeP.Location = new System.Drawing.Point(12, 0);
            this.lblDaNeP.Name = "lblDaNeP";
            this.lblDaNeP.Size = new System.Drawing.Size(304, 92);
            this.lblDaNeP.TabIndex = 0;
            this.lblDaNeP.Text = "Pitanje da/ne";
            this.lblDaNeP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rbtDaNePDa
            // 
            this.rbtDaNePDa.AutoSize = true;
            this.rbtDaNePDa.Location = new System.Drawing.Point(351, 36);
            this.rbtDaNePDa.Name = "rbtDaNePDa";
            this.rbtDaNePDa.Size = new System.Drawing.Size(47, 21);
            this.rbtDaNePDa.TabIndex = 2;
            this.rbtDaNePDa.TabStop = true;
            this.rbtDaNePDa.Text = "Da";
            this.rbtDaNePDa.UseVisualStyleBackColor = true;
            this.rbtDaNePDa.CheckedChanged += new System.EventHandler(this.rbtDaNePDa_CheckedChanged);
            // 
            // rbtDaNePNe
            // 
            this.rbtDaNePNe.AutoSize = true;
            this.rbtDaNePNe.Location = new System.Drawing.Point(415, 36);
            this.rbtDaNePNe.Name = "rbtDaNePNe";
            this.rbtDaNePNe.Size = new System.Drawing.Size(47, 21);
            this.rbtDaNePNe.TabIndex = 2;
            this.rbtDaNePNe.TabStop = true;
            this.rbtDaNePNe.Text = "Ne";
            this.rbtDaNePNe.UseVisualStyleBackColor = true;
            this.rbtDaNePNe.CheckedChanged += new System.EventHandler(this.rbtDaNePNe_CheckedChanged);
            // 
            // PitanjeDaNePControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rbtDaNePNe);
            this.Controls.Add(this.rbtDaNePDa);
            this.Controls.Add(this.lblDaNeP);
            this.Name = "PitanjeDaNePControl";
            this.Size = new System.Drawing.Size(516, 103);
            this.Load += new System.EventHandler(this.PitanjeDaNePControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDaNeP;
        private System.Windows.Forms.RadioButton rbtDaNePDa;
        private System.Windows.Forms.RadioButton rbtDaNePNe;
    }
}
