﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Prikaz
{
    public partial class ListaIntPControl : UserControl
    {
        public PitanjeListaInt pitanje;
        public Form roditelj;
        public bool daLiUpisujemoVrednost = false;

        public ListaIntPControl()
        {
            InitializeComponent();
        }

        private void InicijalizujKomponente()
        {
            if ((lblPitanjeListaIntP == null) || (cbxPitanjeListaIntP == null))
                return;
            lblPitanjeListaIntP.Text = pitanje.tekstPitanja;
            cbxPitanjeListaIntP.DataSource = pitanje.ponudjeniOdgovoriInt.ToArray();
            cbxPitanjeListaIntP.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        private void ListaIntPControl_Load(object sender, EventArgs e)
        {
            InicijalizujKomponente();
            OsveziKomponentu();
        }

        private void cbxPitanjeListaIntP_SelectedIndexChanged(object sender, EventArgs e)
        {
            PostavljanjeVrednostiCBX();
        }

        private void PostavljanjeVrednostiCBX()
        {
            var pom = roditelj as FormPopuniAnketu;
            for (int i = 0; i < pom.anketa.listaPitanja.Count; i++)
            {
                if (pom.anketa.listaPitanja[i].tekstPitanja == lblPitanjeListaIntP.Text && pom.anketa.listaPitanja[i].tip == tipPitanja.listaBrojeva)
                {
                    if (daLiUpisujemoVrednost)
                    {
                        (pom.anketa.listaPitanja[i] as PitanjeListaInt).odgovor = (int)cbxPitanjeListaIntP.SelectedValue;
                        daLiUpisujemoVrednost = false;
                    }
                }
            }
        }

        private void OsveziKomponentu()
        {
            if (pitanje != null)
            {
                for (int i = 0; i < cbxPitanjeListaIntP.Items.Count; i++)
                {
                    if (cbxPitanjeListaIntP.GetItemText(cbxPitanjeListaIntP.Items[i]) == pitanje.odgovor.ToString())
                    {
                        cbxPitanjeListaIntP.SelectedIndex = i;
                    }
                }
            }
        }

        private void cbxPitanjeListaIntP_SelectionChangeCommitted(object sender, EventArgs e)
        {
            daLiUpisujemoVrednost = true;
            PostavljanjeVrednostiCBX();
        }
    }
}
