﻿namespace Anketa1389.Controls.Prikaz
{
    partial class PitanjeStringPControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPitanjeSlobodanUnosP = new System.Windows.Forms.Label();
            this.txtSlobodanUnosP = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblPitanjeSlobodanUnosP
            // 
            this.lblPitanjeSlobodanUnosP.Location = new System.Drawing.Point(3, 0);
            this.lblPitanjeSlobodanUnosP.Name = "lblPitanjeSlobodanUnosP";
            this.lblPitanjeSlobodanUnosP.Size = new System.Drawing.Size(293, 92);
            this.lblPitanjeSlobodanUnosP.TabIndex = 0;
            this.lblPitanjeSlobodanUnosP.Text = "Pitanje String";
            this.lblPitanjeSlobodanUnosP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSlobodanUnosP
            // 
            this.txtSlobodanUnosP.Location = new System.Drawing.Point(303, 4);
            this.txtSlobodanUnosP.MaxLength = 114;
            this.txtSlobodanUnosP.Multiline = true;
            this.txtSlobodanUnosP.Name = "txtSlobodanUnosP";
            this.txtSlobodanUnosP.Size = new System.Drawing.Size(188, 88);
            this.txtSlobodanUnosP.TabIndex = 1;
            this.txtSlobodanUnosP.TextChanged += new System.EventHandler(this.txtSlobodanUnosP_TextChanged);
            // 
            // PitanjeStringPControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtSlobodanUnosP);
            this.Controls.Add(this.lblPitanjeSlobodanUnosP);
            this.Name = "PitanjeStringPControl";
            this.Size = new System.Drawing.Size(506, 103);
            this.Load += new System.EventHandler(this.SlobodanUnosPControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPitanjeSlobodanUnosP;
        private System.Windows.Forms.TextBox txtSlobodanUnosP;
    }
}
