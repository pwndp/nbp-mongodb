﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Prikaz
{
    public partial class PitanjeDaNePControl : UserControl
    {
        public PitanjeDaNe pitanje;
        public Form roditelj;
        public static bool daLiUpisujemoVrednost = false;

        public PitanjeDaNePControl()
        {
            InitializeComponent();
        }

        private void InicijalizujKomponente()
        {
            if ((lblDaNeP == null) || (rbtDaNePDa == null) || (rbtDaNePNe == null))
                return;
            lblDaNeP.Text = pitanje.tekstPitanja;
            if ((daLiUpisujemoVrednost == false) || (pitanje.odgovor == null))
            {
                rbtDaNePDa.Checked = false;  //Defaultno je "Da" - vise nije!
                rbtDaNePNe.Checked = false;
            }
            else
            {
                if (pitanje.odgovor!=null && pitanje.odgovor==true)
                {
                    rbtDaNePDa.Checked = true;
                    rbtDaNePNe.Checked = false;
                }
                else
                {
                    rbtDaNePDa.Checked = false;
                    rbtDaNePNe.Checked = true;
                }
            }

        }

        private void PitanjeDaNePControl_Load(object sender, EventArgs e)
        {
            InicijalizujKomponente();
        }

        private void rbtDaNePDa_CheckedChanged(object sender, EventArgs e)
        {
            var pom = roditelj as FormPopuniAnketu;

            for (int i = 0; i < pom.anketa.listaPitanja.Count; i++)
            {
                if (pom.anketa.listaPitanja[i].tekstPitanja == lblDaNeP.Text && pom.anketa.listaPitanja[i].tip == tipPitanja.daNe)
                {
                    if (rbtDaNePDa.Enabled)
                    {
                        (pom.anketa.listaPitanja[i] as PitanjeDaNe).odgovor = true;
                        daLiUpisujemoVrednost = true;
                    }
                }
            }
        }

        private void rbtDaNePNe_CheckedChanged(object sender, EventArgs e)
        {
            var pom = roditelj as FormPopuniAnketu;

            for (int i = 0; i < pom.anketa.listaPitanja.Count; i++)
            {
                if (pom.anketa.listaPitanja[i].tekstPitanja == lblDaNeP.Text && pom.anketa.listaPitanja[i].tip == tipPitanja.daNe)
                {
                    if (rbtDaNePNe.Enabled)
                    {
                        (pom.anketa.listaPitanja[i] as PitanjeDaNe).odgovor = false;
                        daLiUpisujemoVrednost = true;
                    }
                }
            }
        }
    }
}
