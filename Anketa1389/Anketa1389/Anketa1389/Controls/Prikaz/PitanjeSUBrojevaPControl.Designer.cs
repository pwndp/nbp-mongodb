﻿namespace Anketa1389.Controls.Prikaz
{
    partial class PitanjeSUBrojevaPControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSUBrojevaP = new System.Windows.Forms.Label();
            this.txtSUBrojevaP = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSUBrojevaP
            // 
            this.lblSUBrojevaP.Location = new System.Drawing.Point(3, 0);
            this.lblSUBrojevaP.Name = "lblSUBrojevaP";
            this.lblSUBrojevaP.Size = new System.Drawing.Size(293, 92);
            this.lblSUBrojevaP.TabIndex = 0;
            this.lblSUBrojevaP.Text = "Pitanje slobodan unos broja";
            this.lblSUBrojevaP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSUBrojevaP
            // 
            this.txtSUBrojevaP.Location = new System.Drawing.Point(342, 35);
            this.txtSUBrojevaP.MaxLength = 10;
            this.txtSUBrojevaP.Name = "txtSUBrojevaP";
            this.txtSUBrojevaP.Size = new System.Drawing.Size(100, 22);
            this.txtSUBrojevaP.TabIndex = 1;
            this.txtSUBrojevaP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSUBrojevaP.TextChanged += new System.EventHandler(this.txtSUBrojevaP_TextChanged);
            this.txtSUBrojevaP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSUBrojevaP_KeyPress);
            // 
            // PitanjeSUBrojevaPControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtSUBrojevaP);
            this.Controls.Add(this.lblSUBrojevaP);
            this.Name = "PitanjeSUBrojevaPControl";
            this.Size = new System.Drawing.Size(516, 103);
            this.Load += new System.EventHandler(this.PitanjeSUBrojevaPControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSUBrojevaP;
        private System.Windows.Forms.TextBox txtSUBrojevaP;
    }
}
