﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Prikaz
{
    public partial class StatistikaAnketaPControl : UserControl
    {
        public Form roditelj;
        private Anketa _anketa;
        public Anketa Anketa
        {
            get
            {
                return _anketa;
            }
            set
            {
                _anketa = value;
                initData();
            }
        }
        
        public StatistikaAnketaPControl()
        {
            InitializeComponent();
        }

        private void initData()
        {
            if (Anketa == null)
                return;

            lblNaziv.Text = Anketa.naziv;
            lblOpis.Text = Anketa.opis;

            if (Anketa.daLiJeAktivna)
            {
                btnObrisi.Visible = false;
                btnObrisi.Enabled = false;
                lblObrisi.Visible = false;
            }

        }

        private void btnStatistika_Click(object sender, EventArgs e)
        {
            FormPogledajRezultate frm = new FormPogledajRezultate();
            frm.client = (roditelj as FormProfil).client;
            frm.korisnik = (roditelj as FormProfil).Korisnik;
            frm.Anketa = Anketa;
            frm.ShowDialog();

            (roditelj as FormProfil).Korisnik = (roditelj as FormProfil).client.VratiKorisnikovePodatke((roditelj as FormProfil).Korisnik);
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Jeste li sigurni da zelite da uklonite odabranu anketu (ova akcija je trajna i nepovratna) ?","",MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dr == DialogResult.Yes)
            {
                var rod = roditelj as FormProfil;
                for (int i = 0; i < rod.Korisnik.kreiraneAnkete.Count; i++)
                {
                    if (rod.Korisnik.kreiraneAnkete[i].naziv == Anketa.naziv && rod.Korisnik.kreiraneAnkete[i].opis == Anketa.opis)
                    {
                        rod.Korisnik.kreiraneAnkete.RemoveAt(i);
                        rod.client.AzurirajKorisnika(rod.Korisnik);
                        rod.client.UkloniAnketu(Anketa);
                        rod.Korisnik = rod.client.VratiKorisnikovePodatke(rod.Korisnik); // Dodeli ponovo da osvezis i resetujes pogled i podatke
                    }
                }
            }
        }
    }
}
