﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Prikaz
{
    public partial class PitanjeStringPControl : UserControl
    {
        public PitanjeString pitanje;
        public Form roditelj;

        public PitanjeStringPControl()
        {
            InitializeComponent();
        }

        private void InicijalizujKomponente()
        {
            if ((lblPitanjeSlobodanUnosP == null) || (txtSlobodanUnosP == null))
                return;
            lblPitanjeSlobodanUnosP.Text = pitanje.tekstPitanja;
            txtSlobodanUnosP.Text = "";
        }

        private void SlobodanUnosPControl_Load(object sender, EventArgs e)
        {
            InicijalizujKomponente();
            txtSlobodanUnosP.Text = pitanje.odgovor;
        }

        private void txtSlobodanUnosP_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtSlobodanUnosP.Text))
            { return; }

            var pom = roditelj as FormPopuniAnketu;

            for (int i = 0; i < pom.anketa.listaPitanja.Count; i++)
            {
                if (pom.anketa.listaPitanja[i].tekstPitanja == lblPitanjeSlobodanUnosP.Text && pom.anketa.listaPitanja[i].tip == tipPitanja.tekstualno)
                {
                    (pom.anketa.listaPitanja[i] as PitanjeString).odgovor = txtSlobodanUnosP.Text;
                    pitanje.odgovor = txtSlobodanUnosP.Text;
                }
            }
        }
    }
}
