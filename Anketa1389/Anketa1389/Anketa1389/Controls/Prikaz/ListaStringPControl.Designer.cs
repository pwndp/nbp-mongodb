﻿namespace Anketa1389.Controls.Prikaz
{
    partial class ListaStringPControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPitanjeListaStringP = new System.Windows.Forms.Label();
            this.cbxPitanjeListaStringP = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblPitanjeListaStringP
            // 
            this.lblPitanjeListaStringP.Location = new System.Drawing.Point(3, 0);
            this.lblPitanjeListaStringP.Name = "lblPitanjeListaStringP";
            this.lblPitanjeListaStringP.Size = new System.Drawing.Size(246, 92);
            this.lblPitanjeListaStringP.TabIndex = 0;
            this.lblPitanjeListaStringP.Text = "Pitanje lista stringova";
            this.lblPitanjeListaStringP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxPitanjeListaStringP
            // 
            this.cbxPitanjeListaStringP.FormattingEnabled = true;
            this.cbxPitanjeListaStringP.Location = new System.Drawing.Point(259, 35);
            this.cbxPitanjeListaStringP.Name = "cbxPitanjeListaStringP";
            this.cbxPitanjeListaStringP.Size = new System.Drawing.Size(236, 24);
            this.cbxPitanjeListaStringP.TabIndex = 1;
            this.cbxPitanjeListaStringP.SelectedIndexChanged += new System.EventHandler(this.cbxPitanjeListaStringP_SelectedIndexChanged);
            this.cbxPitanjeListaStringP.SelectionChangeCommitted += new System.EventHandler(this.cbxPitanjeListaStringP_SelectionChangeCommitted);
            // 
            // ListaStringPControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbxPitanjeListaStringP);
            this.Controls.Add(this.lblPitanjeListaStringP);
            this.Name = "ListaStringPControl";
            this.Size = new System.Drawing.Size(506, 103);
            this.Load += new System.EventHandler(this.ListaStringPControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPitanjeListaStringP;
        private System.Windows.Forms.ComboBox cbxPitanjeListaStringP;
    }
}
