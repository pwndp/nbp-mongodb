﻿namespace Anketa1389.Controls.Prikaz
{
    partial class PopunjivanjeAnketaPControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOpis = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.btnPopuni = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblOpis
            // 
            this.lblOpis.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpis.Location = new System.Drawing.Point(15, 28);
            this.lblOpis.Name = "lblOpis";
            this.lblOpis.Size = new System.Drawing.Size(253, 50);
            this.lblOpis.TabIndex = 7;
            this.lblOpis.Text = "Ovde ce da ide neki opis ankete zadat od strane korisnika... On ce da bude raspor" +
    "edjen u vise redova. Barem trebalo bi...";
            this.lblOpis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNaziv
            // 
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaziv.Location = new System.Drawing.Point(15, 9);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(253, 21);
            this.lblNaziv.TabIndex = 6;
            this.lblNaziv.Text = "Ovde ide naziv ankete";
            this.lblNaziv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPopuni
            // 
            this.btnPopuni.Location = new System.Drawing.Point(268, 28);
            this.btnPopuni.Name = "btnPopuni";
            this.btnPopuni.Size = new System.Drawing.Size(104, 32);
            this.btnPopuni.TabIndex = 5;
            this.btnPopuni.Text = "Popuni anketu";
            this.btnPopuni.UseVisualStyleBackColor = true;
            this.btnPopuni.Click += new System.EventHandler(this.btnPopuni_Click);
            // 
            // PopunjivanjeAnketaPControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblOpis);
            this.Controls.Add(this.lblNaziv);
            this.Controls.Add(this.btnPopuni);
            this.Name = "PopunjivanjeAnketaPControl";
            this.Size = new System.Drawing.Size(387, 84);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblOpis;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.Button btnPopuni;
    }
}
