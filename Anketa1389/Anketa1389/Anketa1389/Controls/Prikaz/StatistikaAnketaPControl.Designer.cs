﻿namespace Anketa1389.Controls.Prikaz
{
    partial class StatistikaAnketaPControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStatistika = new System.Windows.Forms.Button();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.lblOpis = new System.Windows.Forms.Label();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.lblObrisi = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStatistika
            // 
            this.btnStatistika.Location = new System.Drawing.Point(272, 29);
            this.btnStatistika.Name = "btnStatistika";
            this.btnStatistika.Size = new System.Drawing.Size(104, 32);
            this.btnStatistika.TabIndex = 0;
            this.btnStatistika.Text = "Prikazi statistiku";
            this.btnStatistika.UseVisualStyleBackColor = true;
            this.btnStatistika.Click += new System.EventHandler(this.btnStatistika_Click);
            // 
            // lblNaziv
            // 
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaziv.Location = new System.Drawing.Point(19, 10);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(253, 21);
            this.lblNaziv.TabIndex = 1;
            this.lblNaziv.Text = "Ovde ide naziv ankete";
            this.lblNaziv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOpis
            // 
            this.lblOpis.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpis.Location = new System.Drawing.Point(19, 29);
            this.lblOpis.Name = "lblOpis";
            this.lblOpis.Size = new System.Drawing.Size(253, 50);
            this.lblOpis.TabIndex = 2;
            this.lblOpis.Text = "Ovde ce da ide neki opis ankete zadat od strane korisnika... On ce da bude raspor" +
    "edjen u vise redova. Barem trebalo bi...";
            this.lblOpis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(356, 8);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(20, 21);
            this.btnObrisi.TabIndex = 3;
            this.btnObrisi.Text = "X";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // lblObrisi
            // 
            this.lblObrisi.AutoSize = true;
            this.lblObrisi.Location = new System.Drawing.Point(281, 12);
            this.lblObrisi.Name = "lblObrisi";
            this.lblObrisi.Size = new System.Drawing.Size(69, 13);
            this.lblObrisi.TabIndex = 4;
            this.lblObrisi.Text = "Obrisi anketu";
            // 
            // StatistikaAnketaPControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblObrisi);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.lblOpis);
            this.Controls.Add(this.lblNaziv);
            this.Controls.Add(this.btnStatistika);
            this.Name = "StatistikaAnketaPControl";
            this.Size = new System.Drawing.Size(387, 84);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStatistika;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.Label lblOpis;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Label lblObrisi;
    }
}
