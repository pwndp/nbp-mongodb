﻿namespace Anketa1389.Controls.Prikaz
{
    partial class ListaIntPControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPitanjeListaIntP = new System.Windows.Forms.Label();
            this.cbxPitanjeListaIntP = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblPitanjeListaIntP
            // 
            this.lblPitanjeListaIntP.Location = new System.Drawing.Point(3, 0);
            this.lblPitanjeListaIntP.Name = "lblPitanjeListaIntP";
            this.lblPitanjeListaIntP.Size = new System.Drawing.Size(293, 92);
            this.lblPitanjeListaIntP.TabIndex = 0;
            this.lblPitanjeListaIntP.Text = "Pitanje lista int";
            this.lblPitanjeListaIntP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxPitanjeListaIntP
            // 
            this.cbxPitanjeListaIntP.FormattingEnabled = true;
            this.cbxPitanjeListaIntP.Location = new System.Drawing.Point(336, 34);
            this.cbxPitanjeListaIntP.Name = "cbxPitanjeListaIntP";
            this.cbxPitanjeListaIntP.Size = new System.Drawing.Size(121, 24);
            this.cbxPitanjeListaIntP.TabIndex = 1;
            this.cbxPitanjeListaIntP.SelectedIndexChanged += new System.EventHandler(this.cbxPitanjeListaIntP_SelectedIndexChanged);
            this.cbxPitanjeListaIntP.SelectionChangeCommitted += new System.EventHandler(this.cbxPitanjeListaIntP_SelectionChangeCommitted);
            // 
            // ListaIntPControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbxPitanjeListaIntP);
            this.Controls.Add(this.lblPitanjeListaIntP);
            this.Name = "ListaIntPControl";
            this.Size = new System.Drawing.Size(516, 103);
            this.Load += new System.EventHandler(this.ListaIntPControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPitanjeListaIntP;
        private System.Windows.Forms.ComboBox cbxPitanjeListaIntP;
    }
}
