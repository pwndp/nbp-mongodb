﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Forms;
using Anketa1389.Entities;
using Anketa1389.Controls.Dodavanje;
using Anketa1389.Controls.Prikaz;

namespace Anketa1389.Controls
{
    public partial class PitanjeControl : UserControl
    {
        public Form roditelj;
        public bool daLiSePrikazuje = true;
        public Pitanje pitanje;

        public void Prikaz()
        {
            pnlContainer.Controls.Clear();
            if (daLiSePrikazuje)
            {
                // Moj kod
                // Inicijalizacija tri pitanja (PitanjeControl 1,2,3)
                if (pitanje != null)
                {                    
                    switch (pitanje.tip)
                    {
                        case tipPitanja.tekstualno:
                            PitanjeStringPControl komponenta0 = new PitanjeStringPControl();
                            komponenta0.Location = new Point(0, 0);
                            komponenta0.roditelj = roditelj;
                            komponenta0.pitanje = pitanje as PitanjeString;
                            pnlContainer.Controls.Add(komponenta0);
                            break;

                        case tipPitanja.listaStringova:
                            ListaStringPControl komponenta1 = new ListaStringPControl();
                            komponenta1.Location = new Point(0, 0);
                            komponenta1.roditelj = roditelj;
                            komponenta1.pitanje = pitanje as PitanjeListaStringova;
                            pnlContainer.Controls.Add(komponenta1);
                            break;

                            //Ovo je sema:
                        case tipPitanja.listaBrojeva:
                            ListaIntPControl komponenta2 = new ListaIntPControl();
                            komponenta2.Location = new Point(0, 0);
                            komponenta2.roditelj = roditelj;
                            komponenta2.pitanje = pitanje as PitanjeListaInt;
                            pnlContainer.Controls.Add(komponenta2);
                            break;

                        case tipPitanja.slobodanUnosBrojeva:
                            PitanjeSUBrojevaPControl komponenta3 = new PitanjeSUBrojevaPControl();
                            komponenta3.Location = new Point(0, 0);
                            komponenta3.roditelj = roditelj;
                            komponenta3.pitanje = pitanje as PitanjeSUBrojeva;
                            pnlContainer.Controls.Add(komponenta3); 
                            break;

                        case tipPitanja.daNe:
                            PitanjeDaNePControl komponenta4 = new PitanjeDaNePControl();
                            komponenta4.Location = new Point(0, 0);
                            komponenta4.roditelj = roditelj;
                            komponenta4.pitanje = pitanje as PitanjeDaNe;
                            pnlContainer.Controls.Add(komponenta4);
                            break;
                    }

                }

            }
            else
            {
		    if (pitanje != null)
                {
                    switch (pitanje.tip)
                    {
                        case tipPitanja.tekstualno:
                            PitanjeStringDControl kon1 = new PitanjeStringDControl();
                            kon1.pitanje = pitanje as PitanjeString;
                            kon1.Location = new Point(0, 0);
                            kon1.roditelj = roditelj;
                            pnlContainer.Controls.Add(kon1);
                            break;
                        case tipPitanja.listaStringova:
                            ListaStringDControl kon2 = new ListaStringDControl();
                            kon2.pitanje = pitanje as PitanjeListaStringova;
                            kon2.Location = new Point(0, 0);
                            kon2.roditelj = roditelj;
                            pnlContainer.Controls.Add(kon2);
                            break;
                        case tipPitanja.listaBrojeva:
                            ListaIntDControl kon3 = new ListaIntDControl();
                            kon3.pitanje = pitanje as PitanjeListaInt;
                            kon3.Location = new Point(0, 0);
                            kon3.roditelj = roditelj;
                            pnlContainer.Controls.Add(kon3);
                            break;
                        case tipPitanja.slobodanUnosBrojeva:
                            PitanjeSUBrojevaDControl kon4 = new PitanjeSUBrojevaDControl();
                            kon4.pitanje = pitanje as PitanjeSUBrojeva;
                            kon4.Location = new Point(0, 0);
                            kon4.roditelj = roditelj;
                            pnlContainer.Controls.Add(kon4);
                            break;
                        case tipPitanja.daNe:
                            PitanjeDaNeDControl kon5 = new PitanjeDaNeDControl();
                            kon5.Size = this.Size;
                            kon5.pitanje = pitanje as PitanjeDaNe;
                            kon5.Location = new Point(0, 0);
                            kon5.roditelj = roditelj;
                            pnlContainer.Controls.Add(kon5);
                            break;
                    }
                }

            }
        }

        public PitanjeControl()
        {
            InitializeComponent();
        }


    }
}
