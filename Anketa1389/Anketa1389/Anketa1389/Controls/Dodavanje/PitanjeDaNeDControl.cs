﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Dodavanje
{
    public partial class PitanjeDaNeDControl : UserControl
    {
        public Form roditelj;
        private PitanjeDaNe _pitanje;
        public PitanjeDaNe pitanje
        {
            get
            {
                return _pitanje;
            }
            set
            {
                _pitanje = value;
                initData();
            }
        }
        private bool settingCheckState=false;

        public PitanjeDaNeDControl()
        {
            InitializeComponent();
            pitanje = new PitanjeDaNe();
        }

        private void PitanjeDaNeDControl_Load(object sender, EventArgs e)
        {
            initData();
        }

        private void initData()
        {
            if (pitanje == null)
                return;
            tbxPitanje.Text = pitanje.tekstPitanja;
            settingCheckState = true;
            ckbxObavezno.Checked = pitanje.daLiJeObavezno;
            settingCheckState = false;
        }

        private void tbxPitanje_Leave(object sender, EventArgs e)
        {
            pitanje.tekstPitanja = tbxPitanje.Text;
            setValues();
        }

        private void ckbxObavezno_CheckedChanged(object sender, EventArgs e)
        {
            if (settingCheckState)
                return;
            pitanje.daLiJeObavezno = ckbxObavezno.Checked;
            setValues();
        }

        private void setValues()
        {
            var par = roditelj as FormNapraviAnketu;
            if (par.currentID != -1)
                return;
            par.anketa.listaPitanja[par.currentID].tekstPitanja = pitanje.tekstPitanja;
            par.anketa.listaPitanja[par.currentID].daLiJeObavezno = pitanje.daLiJeObavezno;
        }
    }
}
