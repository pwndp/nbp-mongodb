﻿namespace Anketa1389.Controls.Dodavanje
{
    partial class PitanjeSUBrojevaDControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPitanje = new System.Windows.Forms.Label();
            this.tbxPitanje = new System.Windows.Forms.TextBox();
            this.ckbxObavezno = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblPitanje
            // 
            this.lblPitanje.AutoSize = true;
            this.lblPitanje.Location = new System.Drawing.Point(187, 17);
            this.lblPitanje.Name = "lblPitanje";
            this.lblPitanje.Size = new System.Drawing.Size(166, 13);
            this.lblPitanje.TabIndex = 6;
            this.lblPitanje.Text = "Tekst pitanja (Numericki odgovor)";
            // 
            // tbxPitanje
            // 
            this.tbxPitanje.Location = new System.Drawing.Point(69, 45);
            this.tbxPitanje.MaxLength = 80;
            this.tbxPitanje.Name = "tbxPitanje";
            this.tbxPitanje.Size = new System.Drawing.Size(379, 20);
            this.tbxPitanje.TabIndex = 5;
            this.tbxPitanje.Leave += new System.EventHandler(this.tbxPitanje_Leave);
            // 
            // ckbxObavezno
            // 
            this.ckbxObavezno.AutoSize = true;
            this.ckbxObavezno.Location = new System.Drawing.Point(224, 74);
            this.ckbxObavezno.Name = "ckbxObavezno";
            this.ckbxObavezno.Size = new System.Drawing.Size(109, 17);
            this.ckbxObavezno.TabIndex = 7;
            this.ckbxObavezno.Text = "Obavezno pitanje";
            this.ckbxObavezno.UseVisualStyleBackColor = true;
            this.ckbxObavezno.CheckedChanged += new System.EventHandler(this.ckbxObavezno_CheckedChanged);
            // 
            // PitanjeSUBrojevaDControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ckbxObavezno);
            this.Controls.Add(this.lblPitanje);
            this.Controls.Add(this.tbxPitanje);
            this.Name = "PitanjeSUBrojevaDControl";
            this.Size = new System.Drawing.Size(516, 103);
            this.Load += new System.EventHandler(this.PitanjeSUBrojevaDControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPitanje;
        private System.Windows.Forms.TextBox tbxPitanje;
        private System.Windows.Forms.CheckBox ckbxObavezno;
    }
}
