﻿namespace Anketa1389.Controls.Dodavanje
{
    partial class ListaStringDControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOdgovori = new System.Windows.Forms.Label();
            this.lblPitanje = new System.Windows.Forms.Label();
            this.tbxOpcije = new System.Windows.Forms.TextBox();
            this.tbxPitanje = new System.Windows.Forms.TextBox();
            this.ckbxObavezno = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblOdgovori
            // 
            this.lblOdgovori.Location = new System.Drawing.Point(6, 58);
            this.lblOdgovori.Name = "lblOdgovori";
            this.lblOdgovori.Size = new System.Drawing.Size(128, 29);
            this.lblOdgovori.TabIndex = 7;
            this.lblOdgovori.Text = "Ponudjeni odgovori (odvojeni tacka-zarezom)";
            this.lblOdgovori.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPitanje
            // 
            this.lblPitanje.AutoSize = true;
            this.lblPitanje.Location = new System.Drawing.Point(33, 26);
            this.lblPitanje.Name = "lblPitanje";
            this.lblPitanje.Size = new System.Drawing.Size(68, 13);
            this.lblPitanje.TabIndex = 6;
            this.lblPitanje.Text = "Tekst pitanja";
            // 
            // tbxOpcije
            // 
            this.tbxOpcije.Location = new System.Drawing.Point(140, 63);
            this.tbxOpcije.MaxLength = 88;
            this.tbxOpcije.Name = "tbxOpcije";
            this.tbxOpcije.Size = new System.Drawing.Size(364, 20);
            this.tbxOpcije.TabIndex = 5;
            this.tbxOpcije.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxOpcije_KeyPress);
            this.tbxOpcije.Leave += new System.EventHandler(this.tbxOpcije_Leave);
            // 
            // tbxPitanje
            // 
            this.tbxPitanje.Location = new System.Drawing.Point(140, 23);
            this.tbxPitanje.MaxLength = 127;
            this.tbxPitanje.Name = "tbxPitanje";
            this.tbxPitanje.Size = new System.Drawing.Size(249, 20);
            this.tbxPitanje.TabIndex = 4;
            this.tbxPitanje.Leave += new System.EventHandler(this.tbxPitanje_Leave);
            // 
            // ckbxObavezno
            // 
            this.ckbxObavezno.AutoSize = true;
            this.ckbxObavezno.Location = new System.Drawing.Point(395, 25);
            this.ckbxObavezno.Name = "ckbxObavezno";
            this.ckbxObavezno.Size = new System.Drawing.Size(109, 17);
            this.ckbxObavezno.TabIndex = 10;
            this.ckbxObavezno.Text = "Obavezno pitanje";
            this.ckbxObavezno.UseVisualStyleBackColor = true;
            this.ckbxObavezno.CheckedChanged += new System.EventHandler(this.ckbxObavezno_CheckedChanged);
            // 
            // ListaStringDControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ckbxObavezno);
            this.Controls.Add(this.lblOdgovori);
            this.Controls.Add(this.lblPitanje);
            this.Controls.Add(this.tbxOpcije);
            this.Controls.Add(this.tbxPitanje);
            this.Name = "ListaStringDControl";
            this.Size = new System.Drawing.Size(516, 103);
            this.Load += new System.EventHandler(this.ListaStringDControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOdgovori;
        private System.Windows.Forms.Label lblPitanje;
        private System.Windows.Forms.TextBox tbxOpcije;
        private System.Windows.Forms.TextBox tbxPitanje;
        private System.Windows.Forms.CheckBox ckbxObavezno;
    }
}
