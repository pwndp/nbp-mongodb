﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Dodavanje
{
    public partial class ListaStringDControl : UserControl
    {
        public Form roditelj;
        private PitanjeListaStringova _pitanje;
        public PitanjeListaStringova pitanje
        {
            get
            {
                return _pitanje;
            }
            set
            {
                _pitanje = value;
                initData();
            }
        }
        private bool settingCheckState = false;

        public ListaStringDControl()
        {
            InitializeComponent();
            List<string> tmpList = new List<string>();
            pitanje = new PitanjeListaStringova(tmpList);
        }

        private void tbxOpcije_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsLetterOrDigit(e.KeyChar) || e.KeyChar==';' || Char.IsWhiteSpace(e.KeyChar) || Char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
                return;
            }
            pitanje.ponudjeniOdgovoriString = (!String.IsNullOrWhiteSpace(tbxOpcije.Text)) ? new List<string>(tbxOpcije.Text.Split(';').Where(x => !String.IsNullOrWhiteSpace(x))) : new List<string>();
            setValues();
        }

        private void ListaStringDControl_Load(object sender, EventArgs e)
        {
            initData();
        }

        private void initData()
        {
            if (pitanje == null)
                return;
            tbxPitanje.Text = pitanje.tekstPitanja;
            string txt = (pitanje.ponudjeniOdgovoriString.Count > 0) ? pitanje.ponudjeniOdgovoriString.ToArray().Aggregate((cur, nxt) => cur + ";" + nxt) : "";
            tbxOpcije.Text = txt;
            settingCheckState = true;
            ckbxObavezno.Checked = pitanje.daLiJeObavezno;
            settingCheckState = false;
        }

        private void tbxOpcije_Leave(object sender, EventArgs e)
        {
            pitanje.ponudjeniOdgovoriString = (!String.IsNullOrWhiteSpace(tbxOpcije.Text)) ? new List<string>(tbxOpcije.Text.Split(';').Where(x=>!String.IsNullOrWhiteSpace(x))) : new List<string>();
            setValues();
        }

        private void ckbxObavezno_CheckedChanged(object sender, EventArgs e)
        {
            if (settingCheckState)
                return;
            pitanje.daLiJeObavezno = ckbxObavezno.Checked;
            setValues();
        }

        private void tbxPitanje_Leave(object sender, EventArgs e)
        {
            pitanje.tekstPitanja = tbxPitanje.Text;
            setValues();
        }

        private void setValues()
        {
            var par = roditelj as FormNapraviAnketu;
            if (par.currentID != -1)
                return;
            par.anketa.listaPitanja[par.currentID].tekstPitanja = pitanje.tekstPitanja;
            par.anketa.listaPitanja[par.currentID].daLiJeObavezno = pitanje.daLiJeObavezno;
            (par.anketa.listaPitanja[par.currentID] as PitanjeListaStringova).ponudjeniOdgovoriString = pitanje.ponudjeniOdgovoriString;
        }
    }
}
