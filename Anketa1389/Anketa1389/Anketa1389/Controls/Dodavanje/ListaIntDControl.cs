﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Forms;

namespace Anketa1389.Controls.Dodavanje
{
    public partial class ListaIntDControl : UserControl
    {
        public Form roditelj;
        private PitanjeListaInt _pitanje;
        public PitanjeListaInt pitanje
        {
            get
            {
                return _pitanje;
            }
            set
            {
                _pitanje = value;
                initData();
            }
        }
        private bool settingCheckState = false;

        public ListaIntDControl()
        {
            InitializeComponent();
            List<int> tempLista = new List<int>();
            pitanje = new PitanjeListaInt(tempLista);
        }

        private void ListaIntDControl_Load(object sender, EventArgs e)
        {
            initData();
        }

        public void initData()
        {
            if (pitanje == null)
                return;
            string txt = (pitanje.ponudjeniOdgovoriInt.Count() > 0) ? pitanje.ponudjeniOdgovoriInt.Select(x => x.ToString()).Aggregate((cur, nxt) => cur + " " + nxt) : "";
            tbxOpcije.Text = txt;
            tbxPitanje.Text = pitanje.tekstPitanja;
            settingCheckState = true;
            ckbxObavezno.Checked = pitanje.daLiJeObavezno;
            settingCheckState = false;
        }

        private void tbxOpcije_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar) || Char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
                return;
            }
            
            pitanje.ponudjeniOdgovoriInt = (!String.IsNullOrWhiteSpace(tbxOpcije.Text)) ? new List<int>(tbxOpcije.Text.Split(' ').Where(x=>!String.IsNullOrWhiteSpace(x)).Select( x => Int32.Parse(x) )) : new List<int>();
            setValues();
           
        }

        private void tbxOpcije_Leave(object sender, EventArgs e)
        {
            pitanje.ponudjeniOdgovoriInt = (!String.IsNullOrWhiteSpace(tbxOpcije.Text)) ? new List<int>(tbxOpcije.Text.Split(' ').Where(x => !String.IsNullOrWhiteSpace(x)).Select(x => Int32.Parse(x))) : new List<int>();
            setValues();
        }

        private void ckbxObavezno_CheckedChanged(object sender, EventArgs e)
        {
            if (settingCheckState)
                return;
            pitanje.daLiJeObavezno = ckbxObavezno.Checked;
            setValues();
        }
        
        private void tbxPitanje_Leave(object sender, EventArgs e)
        {
            pitanje.tekstPitanja = tbxPitanje.Text;
            setValues();
        }

        private void setValues()
        {
            var par = roditelj as FormNapraviAnketu;
            if (par.currentID != -1)
                return;
            par.anketa.listaPitanja[par.currentID].tekstPitanja = pitanje.tekstPitanja;
            par.anketa.listaPitanja[par.currentID].daLiJeObavezno = pitanje.daLiJeObavezno;
            (par.anketa.listaPitanja[par.currentID] as PitanjeListaInt).ponudjeniOdgovoriInt = pitanje.ponudjeniOdgovoriInt;
        }

    }
}
