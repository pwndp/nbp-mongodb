﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Entities;
using Anketa1389.Controls.Prikaz;

namespace Anketa1389.Controls
{
    public partial class AnketaControl : UserControl
    {
        public Form roditelj;
        public Anketa Anketa;
        public bool daLiPrikazujemoStatistiku = false;

        public AnketaControl()
        {
            InitializeComponent();
        }

        public void Prikaz()
        {
            pnlContainer.Controls.Clear();

            if (Anketa != null)
            {
                if (!daLiPrikazujemoStatistiku)
                {
                    PopunjivanjeAnketaPControl kont = new PopunjivanjeAnketaPControl();
                    kont.Roditelj = roditelj;
                    kont.Anketa = Anketa;
                    kont.Location = new Point(0, 0);
                    pnlContainer.Controls.Add(kont);
                }
                else
                {
                    StatistikaAnketaPControl konr = new StatistikaAnketaPControl();
                    konr.roditelj = roditelj;
                    konr.Location = new Point(0, 0);
                    konr.Anketa = Anketa;
                    pnlContainer.Controls.Add(konr);
                }
            }
        }

    }
}
