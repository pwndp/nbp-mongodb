﻿using Anketa1389.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anketa1389.Forms
{
    public partial class FormPogledajRezultate : Form
    {
        public MongoDBDataLayer client;
        public List<Anketa> listaAnketa;
        public Korisnik korisnik;
        private Anketa _anketa;
        public Anketa Anketa
        {
            get
            {
                return _anketa;
            }
            set
            {
                _anketa = value;
                initData();
            }
        }

        public FormPogledajRezultate()
        {
            InitializeComponent();
        }

        private void initData()
        {
            if (Anketa == null)
                return;

            listaAnketa = client.VratiPopunjeneAnkete(Anketa);
            DataTable dt = new DataTable();

            if (listaAnketa.Count == 0)
            {
                return;
            }
            List<Pitanje> listaPitanja = listaAnketa[0].listaPitanja;

            foreach (Pitanje p in listaPitanja)
                dt.Columns.Add(p.tekstPitanja.ToString());
            

            for (int i = 0; i < listaAnketa.Count; i++)
            {
                string[] array = new string[listaAnketa[i].listaPitanja.Count];

                for (int j = 0; j < listaAnketa[i].listaPitanja.Count; j++)
                {
                    var pom = listaAnketa[i].listaPitanja[j];
                    switch (pom.tip)
                    {
                        case tipPitanja.tekstualno:
                            PitanjeString p0 = pom as PitanjeString;
                            array[j] = (p0.odgovor == null)? "": p0.odgovor;
                            break;

                        case tipPitanja.listaStringova:
                            PitanjeListaStringova p1 = pom as PitanjeListaStringova;
                            array[j] = (p1.odgovor == null) ? "" : p1.odgovor;
                            break;
                        
                        case tipPitanja.listaBrojeva:
                            PitanjeListaInt p2 = pom as PitanjeListaInt;
                            array[j] = (p2.odgovor == null) ? "" : p2.odgovor.ToString();
                            break;

                        case tipPitanja.slobodanUnosBrojeva:
                            PitanjeSUBrojeva p3 = pom as PitanjeSUBrojeva;
                            array[j] = (p3.odgovor == null) ? "" : p3.odgovor.ToString();
                            break;

                        case tipPitanja.daNe:
                            PitanjeDaNe p4 = pom as PitanjeDaNe;
                            if (p4.odgovor == true)
                                array[j] = "Da";
                            else if (p4.odgovor == false)
                                array[j] = "Ne";
                            else
                                array[j] = "";
                            break;
                    }
                }
                dt.Rows.Add(array);
            }
            dgvRezultatiAnkete.DataSource = dt;

        btnZaustaviAnketu.Enabled = Anketa.daLiJeAktivna;
            
        }

        private void btnZaustaviAnketu_Click(object sender, EventArgs e)
        {
            client.StopirajAnketu(Anketa, korisnik);
            btnZaustaviAnketu.Enabled = false;
        }

        private void FormPogledajRezultate_Load(object sender, EventArgs e)
        {
            if (listaAnketa.Count == 0)
            {
                MessageBox.Show("Nema podataka!");
                this.Close();
            }
        }
    }
}
