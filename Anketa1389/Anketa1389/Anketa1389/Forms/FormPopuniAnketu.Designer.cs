﻿namespace Anketa1389.Forms
{
    partial class FormPopuniAnketu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPopuniAnketu));
            this.gbxAnkete = new System.Windows.Forms.GroupBox();
            this.btnLevo = new System.Windows.Forms.Button();
            this.btnDesno = new System.Windows.Forms.Button();
            this.lblNazivAnkete = new System.Windows.Forms.Label();
            this.lblOpisAnkete = new System.Windows.Forms.Label();
            this.btnPosalji = new System.Windows.Forms.Button();
            this.pitanjeControl3 = new Anketa1389.Controls.PitanjeControl();
            this.pitanjeControl2 = new Anketa1389.Controls.PitanjeControl();
            this.pitanjeControl1 = new Anketa1389.Controls.PitanjeControl();
            this.gbxAnkete.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxAnkete
            // 
            this.gbxAnkete.Controls.Add(this.pitanjeControl3);
            this.gbxAnkete.Controls.Add(this.pitanjeControl2);
            this.gbxAnkete.Controls.Add(this.pitanjeControl1);
            this.gbxAnkete.Controls.Add(this.btnLevo);
            this.gbxAnkete.Controls.Add(this.btnDesno);
            this.gbxAnkete.Location = new System.Drawing.Point(19, 186);
            this.gbxAnkete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxAnkete.Name = "gbxAnkete";
            this.gbxAnkete.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxAnkete.Size = new System.Drawing.Size(701, 388);
            this.gbxAnkete.TabIndex = 0;
            this.gbxAnkete.TabStop = false;
            // 
            // btnLevo
            // 
            this.btnLevo.Location = new System.Drawing.Point(16, 190);
            this.btnLevo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLevo.Name = "btnLevo";
            this.btnLevo.Size = new System.Drawing.Size(59, 34);
            this.btnLevo.TabIndex = 3;
            this.btnLevo.Text = "<-";
            this.btnLevo.UseVisualStyleBackColor = true;
            this.btnLevo.Click += new System.EventHandler(this.btnLevo_Click);
            // 
            // btnDesno
            // 
            this.btnDesno.Location = new System.Drawing.Point(627, 190);
            this.btnDesno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDesno.Name = "btnDesno";
            this.btnDesno.Size = new System.Drawing.Size(59, 34);
            this.btnDesno.TabIndex = 4;
            this.btnDesno.Text = "->";
            this.btnDesno.UseVisualStyleBackColor = true;
            this.btnDesno.Click += new System.EventHandler(this.btnDesno_Click);
            // 
            // lblNazivAnkete
            // 
            this.lblNazivAnkete.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNazivAnkete.Location = new System.Drawing.Point(13, 78);
            this.lblNazivAnkete.Name = "lblNazivAnkete";
            this.lblNazivAnkete.Size = new System.Drawing.Size(707, 30);
            this.lblNazivAnkete.TabIndex = 1;
            this.lblNazivAnkete.Text = "Uticaj kompjutera na odrastanje dece";
            this.lblNazivAnkete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOpisAnkete
            // 
            this.lblOpisAnkete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpisAnkete.Location = new System.Drawing.Point(19, 111);
            this.lblOpisAnkete.Name = "lblOpisAnkete";
            this.lblOpisAnkete.Size = new System.Drawing.Size(701, 71);
            this.lblOpisAnkete.TabIndex = 2;
            this.lblOpisAnkete.Text = resources.GetString("lblOpisAnkete.Text");
            this.lblOpisAnkete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPosalji
            // 
            this.btnPosalji.Location = new System.Drawing.Point(319, 590);
            this.btnPosalji.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPosalji.Name = "btnPosalji";
            this.btnPosalji.Size = new System.Drawing.Size(100, 28);
            this.btnPosalji.TabIndex = 3;
            this.btnPosalji.Text = "Posalji";
            this.btnPosalji.UseVisualStyleBackColor = true;
            this.btnPosalji.Click += new System.EventHandler(this.btnPosalji_Click);
            // 
            // pitanjeControl3
            // 
            this.pitanjeControl3.Location = new System.Drawing.Point(80, 255);
            this.pitanjeControl3.Margin = new System.Windows.Forms.Padding(5);
            this.pitanjeControl3.Name = "pitanjeControl3";
            this.pitanjeControl3.Size = new System.Drawing.Size(540, 127);
            this.pitanjeControl3.TabIndex = 2;
            // 
            // pitanjeControl2
            // 
            this.pitanjeControl2.Location = new System.Drawing.Point(80, 146);
            this.pitanjeControl2.Margin = new System.Windows.Forms.Padding(5);
            this.pitanjeControl2.Name = "pitanjeControl2";
            this.pitanjeControl2.Size = new System.Drawing.Size(540, 127);
            this.pitanjeControl2.TabIndex = 1;
            // 
            // pitanjeControl1
            // 
            this.pitanjeControl1.Location = new System.Drawing.Point(75, 22);
            this.pitanjeControl1.Margin = new System.Windows.Forms.Padding(5);
            this.pitanjeControl1.Name = "pitanjeControl1";
            this.pitanjeControl1.Size = new System.Drawing.Size(540, 127);
            this.pitanjeControl1.TabIndex = 0;
            this.pitanjeControl1.Load += new System.EventHandler(this.pitanjeControl1_Load);
            // 
            // FormPopuniAnketu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 633);
            this.Controls.Add(this.btnPosalji);
            this.Controls.Add(this.lblOpisAnkete);
            this.Controls.Add(this.lblNazivAnkete);
            this.Controls.Add(this.gbxAnkete);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(758, 680);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(758, 680);
            this.Name = "FormPopuniAnketu";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Anketa";
            this.Load += new System.EventHandler(this.FormPopuniAnketu_Load);
            this.gbxAnkete.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxAnkete;
        private System.Windows.Forms.Label lblNazivAnkete;
        private System.Windows.Forms.Label lblOpisAnkete;
        private System.Windows.Forms.Button btnLevo;
        private System.Windows.Forms.Button btnDesno;
        private Controls.PitanjeControl pitanjeControl1;
        private Controls.PitanjeControl pitanjeControl3;
        private Controls.PitanjeControl pitanjeControl2;
        private System.Windows.Forms.Button btnPosalji;
    }
}