﻿using Newtonsoft.Json;
using Anketa1389.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anketa1389.Forms
{
    public partial class FormNapraviAnketu : Form
    {
        public MongoDBDataLayer client;
        public Anketa anketa;
        public int currentID; // ID pitanja koje se trenutno izmenjuje
        public FormProfil Roditelj;

        public FormNapraviAnketu()
        {
            InitializeComponent();
            
            initData();
        }

        #region Ucitavanje i provere

        private void initData()
        {
            anketa = new Anketa();
            pcPitanje.daLiSePrikazuje = false; // Ne prikazujemo pitanje, vec dodajemo
            pcPitanje.roditelj = this;
            currentID = -1;
            string[] tipovi = { "Tekstualni odabir", "Numericki odabir", "Tekstualni odgovor", "Numericki odgovor", "Da/Ne odgovor" };
            cbxTipPitanja.DataSource = tipovi;
            rebindujListuPitanja();
        }

        private void rebindujListuPitanja()
        {
            cbxPitanja.DataSource = anketa.listaPitanja.Select((Pitanje x) => "Pitanje " + (anketa.listaPitanja.IndexOf(x) + 1)).ToList();
        }

        private bool checkData()
        {
            // Provera naziva, opisa odnosno tagova ankete
            if (String.IsNullOrWhiteSpace(anketa.naziv))
            {
                MessageBox.Show("Anketa mora imati naziv.");
                return false;
            }

            if (String.IsNullOrWhiteSpace(anketa.opis))
            {
                MessageBox.Show("Anketa mora imati opis.");
                return false;
            }

            if (String.IsNullOrWhiteSpace(anketa.tagovi))
            {
                MessageBox.Show("Anketa mora imati bar jedan tag.");
                return false;
            }

            // Provera broja pitanja
            if (anketa.listaPitanja.Count < 1)
            {
                MessageBox.Show("Anketa mora imati bar jedno pitanje.");
                return false;
            }

            // Provera broja obaveznih pitanja
            int obavezni = 0;
            foreach (Pitanje p in anketa.listaPitanja)
                if (p.daLiJeObavezno)
                    obavezni++;

            if (obavezni < 1)
            {
                MessageBox.Show("Anketa mora imati bar jedno obavezno pitanje.");
                return false;
            }

            // Provera strukture pitanja
            for (int i = 0; i < anketa.listaPitanja.Count; i++)
            {
                if (String.IsNullOrWhiteSpace(anketa.listaPitanja[i].tekstPitanja))
                {
                    MessageBox.Show("Pitanje " + (i + 1) + " nema tekst.");
                    return false;
                }

                if (anketa.listaPitanja[i].tip == tipPitanja.listaStringova || anketa.listaPitanja[i].tip == tipPitanja.listaBrojeva)
                {
                    if (anketa.listaPitanja[i].tip == tipPitanja.listaStringova)
                    {
                        PitanjeListaStringova pit = anketa.listaPitanja[i] as PitanjeListaStringova;
                        var txt = (pit.ponudjeniOdgovoriString.Count>0)? pit.ponudjeniOdgovoriString.Select(x => x).ToArray().Aggregate((cur, nxt) => cur + ";" + nxt):"";
                        if (String.IsNullOrWhiteSpace(txt))
                        {
                            MessageBox.Show("Pitanje " + (i + 1) + " nema ponudjene odgovore.");
                            return false;
                        }
                        if (pit.ponudjeniOdgovoriString.Count < 2)
                        {
                            MessageBox.Show("Pitanje " + (i + 1) + " mora da ima bar dva ponudjena odgovora.");
                            return false;
                        }
                    }
                    else if (anketa.listaPitanja[i].tip == tipPitanja.listaBrojeva)
                    {
                        PitanjeListaInt pit = anketa.listaPitanja[i] as PitanjeListaInt;
                        var txt = (pit.ponudjeniOdgovoriInt.Count > 0) ? pit.ponudjeniOdgovoriInt.Select(x => x.ToString()).ToArray().Aggregate((cur, nxt) => cur + " " + nxt) : "";
                        if (String.IsNullOrWhiteSpace(txt))
                        {
                            MessageBox.Show("Pitanje " + (i + 1) + " nema ponudjene odgovore.");
                            return false;
                        }
                        if (pit.ponudjeniOdgovoriInt.Count < 2)
                        {
                            MessageBox.Show("Pitanje " + (i + 1) + " mora da ima bar dva ponudjena odgovora.");
                            return false;
                        }
                    }
                }
            }

            for (int i = 0; i < anketa.listaPitanja.Count-1; i++)
            {
                for (int j = i+1; j < anketa.listaPitanja.Count; j++)
                {
                    if (anketa.listaPitanja[i].tekstPitanja == anketa.listaPitanja[j].tekstPitanja)
                    {
                        MessageBox.Show("Tekstovi pitanja " + (i+1) + " i " + (j+1) + " su isti.");
                        return false;
                    }
                }
            }

            return true;
        }
        
        #endregion

        #region Events

        private void btnKreirajAnketu_Click(object sender, EventArgs e)
        {
            if (!checkData())
                return;

            anketa.naziv = tbxNaziv.Text;
            anketa.opis = tbxOpis.Text;
            anketa.tagovi = tbxOznake.Text;
            anketa.brojUcesnika = 0;
            anketa.daLiJeAktivna = true;
            anketa.mid = 0; // ovaj ID ce se automatski azurirati u bazi

            var tmpkor = new Korisnik() { username = Roditelj.Korisnik.username, password = Roditelj.Korisnik.password };
            client.DodajAnketu(anketa, tmpkor);

            // Zatvori formu
            Roditelj.Korisnik = client.VratiKorisnikovePodatke(tmpkor);
            DialogResult = DialogResult.Yes;
            Close();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            switch (cbxTipPitanja.SelectedIndex)
            {
                case 0:
                    PitanjeListaStringova p1 = new PitanjeListaStringova(new List<string>());
                    anketa.DodajPitanje(p1);
                    break;
                case 1:
                    PitanjeListaInt p2 = new PitanjeListaInt(new List<int>());
                    anketa.DodajPitanje(p2);
                    break;
                case 2:
                    PitanjeString p3 = new PitanjeString();
                    anketa.DodajPitanje(p3);
                    break;
                case 3:
                    PitanjeSUBrojeva p4 = new PitanjeSUBrojeva();
                    anketa.DodajPitanje(p4);
                    break;
                case 4:
                    PitanjeDaNe p5 = new PitanjeDaNe();
                    anketa.DodajPitanje(p5);
                    break;
            }

            rebindujListuPitanja();
            currentID = anketa.listaPitanja.Count - 1;
            pcPitanje.pitanje = anketa.listaPitanja[currentID];
            pcPitanje.Prikaz();
            cbxPitanja.SelectedIndex = currentID;
        }

        private void tbxNaziv_Leave(object sender, EventArgs e)
        {
            anketa.naziv = tbxNaziv.Text;
        }

        private void tbxOpis_Leave(object sender, EventArgs e)
        {
            anketa.opis = tbxOpis.Text;
        }

        private void tbxOznake_Leave(object sender, EventArgs e)
        {
            anketa.tagovi = tbxOznake.Text;
        }

        private void cbxPitanja_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentID = cbxPitanja.SelectedIndex;
            pcPitanje.pitanje = anketa.listaPitanja[currentID];
            pcPitanje.Prikaz();
        }

        private void btnUkloni_Click(object sender, EventArgs e)
        {
            if (currentID == -1)
                return;

            anketa.listaPitanja.RemoveAt(currentID);
            rebindujListuPitanja();
            pcPitanje.pitanje = null;
            if (anketa.listaPitanja.Count != 0)
            {
                currentID--;
                if (currentID < 0)
                    currentID = 0;
                pcPitanje.pitanje = anketa.listaPitanja[currentID];
            }
            else
                currentID = -1;
            pcPitanje.Prikaz();
        }

        private void tbxOznake_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsLetterOrDigit(e.KeyChar) || Char.IsControl(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar)))
            {
                e.Handled = true;
                return;
            }
        }
        
        #endregion

    }
}
