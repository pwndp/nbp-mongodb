﻿using Anketa1389.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anketa1389.Forms
{
    public partial class FormRegistracija : Form
    {
        public MongoDBDataLayer client;

        public FormRegistracija()
        {
            InitializeComponent();
        }

        private bool proveraPodataka()
        {
            if (String.IsNullOrWhiteSpace(tbxUsername.Text))
            {
                MessageBox.Show("Korisnicko ime ne moze biti prazno.");
                return false;
            }

            if (String.IsNullOrWhiteSpace(tbxPassword.Text))
            {
                MessageBox.Show("Lozinka ne moze biti prazna.");
                return false;
            }

            if (String.IsNullOrWhiteSpace(tbxPasswordAgain.Text))
            {
                MessageBox.Show("Molimo Vas, potvrdite lozinku.");
                return false;
            }

            if (tbxPassword.Text != tbxPasswordAgain.Text)
            {
                MessageBox.Show("Lozinke se ne podudaraju.");
                return false;
            }

            if (client.ProveriPostojanjeKorisnika(tbxUsername.Text))
            {
                MessageBox.Show("Korisnicko ime je zauzeto.");
                return false;
            }

            return true;
        }

        private void btnRegistracija_Click(object sender, EventArgs e)
        {
            if (!proveraPodataka())
                return;

            Korisnik k = new Korisnik() { username = tbxUsername.Text, password = tbxPassword.Text };
            k.kreiraneAnkete = new List<Anketa>();

            bool suc = client.DodajKorisnika(k);
            if (suc)
            {
                MessageBox.Show("Korisnik uspesno dodat.");
                this.Close();
            }
            else
                MessageBox.Show("Nastala je greska prilikom dodavanja. Pokusajte ponovo.");
        }
    }
}
