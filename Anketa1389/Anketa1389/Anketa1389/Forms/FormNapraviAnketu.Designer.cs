﻿namespace Anketa1389.Forms
{
    partial class FormNapraviAnketu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxNaziv = new System.Windows.Forms.TextBox();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.lblOpis = new System.Windows.Forms.Label();
            this.tbxOpis = new System.Windows.Forms.TextBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnUkloni = new System.Windows.Forms.Button();
            this.cbxPitanja = new System.Windows.Forms.ComboBox();
            this.gbxGornji = new System.Windows.Forms.GroupBox();
            this.lblOznake = new System.Windows.Forms.Label();
            this.tbxOznake = new System.Windows.Forms.TextBox();
            this.cbxTipPitanja = new System.Windows.Forms.ComboBox();
            this.gbxPitanje = new System.Windows.Forms.GroupBox();
            this.pcPitanje = new Anketa1389.Controls.PitanjeControl();
            this.btnKreirajAnketu = new System.Windows.Forms.Button();
            this.gbxGornji.SuspendLayout();
            this.gbxPitanje.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbxNaziv
            // 
            this.tbxNaziv.Location = new System.Drawing.Point(24, 60);
            this.tbxNaziv.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxNaziv.MaxLength = 126;
            this.tbxNaziv.Name = "tbxNaziv";
            this.tbxNaziv.Size = new System.Drawing.Size(300, 22);
            this.tbxNaziv.TabIndex = 0;
            this.tbxNaziv.Leave += new System.EventHandler(this.tbxNaziv_Leave);
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Location = new System.Drawing.Point(131, 31);
            this.lblNaziv.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(90, 17);
            this.lblNaziv.TabIndex = 1;
            this.lblNaziv.Text = "Naziv ankete";
            // 
            // lblOpis
            // 
            this.lblOpis.AutoSize = true;
            this.lblOpis.Location = new System.Drawing.Point(508, 31);
            this.lblOpis.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOpis.Name = "lblOpis";
            this.lblOpis.Size = new System.Drawing.Size(37, 17);
            this.lblOpis.TabIndex = 3;
            this.lblOpis.Text = "Opis";
            // 
            // tbxOpis
            // 
            this.tbxOpis.Location = new System.Drawing.Point(333, 60);
            this.tbxOpis.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxOpis.MaxLength = 373;
            this.tbxOpis.Name = "tbxOpis";
            this.tbxOpis.Size = new System.Drawing.Size(375, 22);
            this.tbxOpis.TabIndex = 1;
            this.tbxOpis.Leave += new System.EventHandler(this.tbxOpis_Leave);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(267, 143);
            this.btnDodaj.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(103, 28);
            this.btnDodaj.TabIndex = 4;
            this.btnDodaj.Text = "Dodaj pitanje";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnUkloni
            // 
            this.btnUkloni.Location = new System.Drawing.Point(597, 142);
            this.btnUkloni.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUkloni.Name = "btnUkloni";
            this.btnUkloni.Size = new System.Drawing.Size(112, 28);
            this.btnUkloni.TabIndex = 6;
            this.btnUkloni.Text = "Ukloni pitanje";
            this.btnUkloni.UseVisualStyleBackColor = true;
            this.btnUkloni.Click += new System.EventHandler(this.btnUkloni_Click);
            // 
            // cbxPitanja
            // 
            this.cbxPitanja.FormattingEnabled = true;
            this.cbxPitanja.Location = new System.Drawing.Point(377, 144);
            this.cbxPitanja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxPitanja.Name = "cbxPitanja";
            this.cbxPitanja.Size = new System.Drawing.Size(211, 24);
            this.cbxPitanja.TabIndex = 5;
            this.cbxPitanja.SelectedIndexChanged += new System.EventHandler(this.cbxPitanja_SelectedIndexChanged);
            // 
            // gbxGornji
            // 
            this.gbxGornji.Controls.Add(this.lblOznake);
            this.gbxGornji.Controls.Add(this.tbxOznake);
            this.gbxGornji.Controls.Add(this.cbxTipPitanja);
            this.gbxGornji.Controls.Add(this.btnDodaj);
            this.gbxGornji.Controls.Add(this.cbxPitanja);
            this.gbxGornji.Controls.Add(this.tbxNaziv);
            this.gbxGornji.Controls.Add(this.btnUkloni);
            this.gbxGornji.Controls.Add(this.lblNaziv);
            this.gbxGornji.Controls.Add(this.tbxOpis);
            this.gbxGornji.Controls.Add(this.lblOpis);
            this.gbxGornji.Location = new System.Drawing.Point(27, 16);
            this.gbxGornji.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxGornji.Name = "gbxGornji";
            this.gbxGornji.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxGornji.Size = new System.Drawing.Size(737, 203);
            this.gbxGornji.TabIndex = 8;
            this.gbxGornji.TabStop = false;
            // 
            // lblOznake
            // 
            this.lblOznake.AutoSize = true;
            this.lblOznake.Location = new System.Drawing.Point(20, 107);
            this.lblOznake.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOznake.Name = "lblOznake";
            this.lblOznake.Size = new System.Drawing.Size(198, 17);
            this.lblOznake.TabIndex = 10;
            this.lblOznake.Text = "Oznake (odvojene razmakom)";
            // 
            // tbxOznake
            // 
            this.tbxOznake.Location = new System.Drawing.Point(223, 103);
            this.tbxOznake.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxOznake.MaxLength = 255;
            this.tbxOznake.Name = "tbxOznake";
            this.tbxOznake.Size = new System.Drawing.Size(485, 22);
            this.tbxOznake.TabIndex = 2;
            this.tbxOznake.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxOznake_KeyPress);
            this.tbxOznake.Leave += new System.EventHandler(this.tbxOznake_Leave);
            // 
            // cbxTipPitanja
            // 
            this.cbxTipPitanja.FormattingEnabled = true;
            this.cbxTipPitanja.Location = new System.Drawing.Point(24, 145);
            this.cbxTipPitanja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxTipPitanja.Name = "cbxTipPitanja";
            this.cbxTipPitanja.Size = new System.Drawing.Size(233, 24);
            this.cbxTipPitanja.TabIndex = 3;
            // 
            // gbxPitanje
            // 
            this.gbxPitanje.Controls.Add(this.pcPitanje);
            this.gbxPitanje.Location = new System.Drawing.Point(28, 226);
            this.gbxPitanje.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPitanje.Name = "gbxPitanje";
            this.gbxPitanje.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPitanje.Size = new System.Drawing.Size(736, 176);
            this.gbxPitanje.TabIndex = 9;
            this.gbxPitanje.TabStop = false;
            // 
            // pcPitanje
            // 
            this.pcPitanje.Location = new System.Drawing.Point(20, 26);
            this.pcPitanje.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.pcPitanje.Name = "pcPitanje";
            this.pcPitanje.Size = new System.Drawing.Size(696, 127);
            this.pcPitanje.TabIndex = 0;
            // 
            // btnKreirajAnketu
            // 
            this.btnKreirajAnketu.Location = new System.Drawing.Point(325, 410);
            this.btnKreirajAnketu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnKreirajAnketu.Name = "btnKreirajAnketu";
            this.btnKreirajAnketu.Size = new System.Drawing.Size(140, 28);
            this.btnKreirajAnketu.TabIndex = 0;
            this.btnKreirajAnketu.Text = "Kreiraj anketu";
            this.btnKreirajAnketu.UseVisualStyleBackColor = true;
            this.btnKreirajAnketu.Click += new System.EventHandler(this.btnKreirajAnketu_Click);
            // 
            // FormNapraviAnketu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 454);
            this.Controls.Add(this.btnKreirajAnketu);
            this.Controls.Add(this.gbxPitanje);
            this.Controls.Add(this.gbxGornji);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(810, 501);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(810, 501);
            this.Name = "FormNapraviAnketu";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Napravi anketu";
            this.gbxGornji.ResumeLayout(false);
            this.gbxGornji.PerformLayout();
            this.gbxPitanje.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbxNaziv;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.Label lblOpis;
        private System.Windows.Forms.TextBox tbxOpis;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnUkloni;
        private System.Windows.Forms.ComboBox cbxPitanja;
        private System.Windows.Forms.GroupBox gbxGornji;
        private System.Windows.Forms.GroupBox gbxPitanje;
        private Controls.PitanjeControl pcPitanje;
        private System.Windows.Forms.ComboBox cbxTipPitanja;
        private System.Windows.Forms.Button btnKreirajAnketu;
        private System.Windows.Forms.Label lblOznake;
        private System.Windows.Forms.TextBox tbxOznake;
    }
}