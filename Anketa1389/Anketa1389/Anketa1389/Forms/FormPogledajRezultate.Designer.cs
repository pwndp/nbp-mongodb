﻿namespace Anketa1389.Forms
{
    partial class FormPogledajRezultate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvRezultatiAnkete = new System.Windows.Forms.DataGridView();
            this.btnZaustaviAnketu = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRezultatiAnkete)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvRezultatiAnkete
            // 
            this.dgvRezultatiAnkete.AllowUserToAddRows = false;
            this.dgvRezultatiAnkete.AllowUserToDeleteRows = false;
            this.dgvRezultatiAnkete.AllowUserToOrderColumns = true;
            this.dgvRezultatiAnkete.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRezultatiAnkete.Location = new System.Drawing.Point(12, 12);
            this.dgvRezultatiAnkete.Name = "dgvRezultatiAnkete";
            this.dgvRezultatiAnkete.ReadOnly = true;
            this.dgvRezultatiAnkete.RowTemplate.Height = 24;
            this.dgvRezultatiAnkete.Size = new System.Drawing.Size(1043, 530);
            this.dgvRezultatiAnkete.TabIndex = 0;
            // 
            // btnZaustaviAnketu
            // 
            this.btnZaustaviAnketu.Location = new System.Drawing.Point(476, 558);
            this.btnZaustaviAnketu.Name = "btnZaustaviAnketu";
            this.btnZaustaviAnketu.Size = new System.Drawing.Size(121, 45);
            this.btnZaustaviAnketu.TabIndex = 1;
            this.btnZaustaviAnketu.Text = "Zaustavi anketu";
            this.btnZaustaviAnketu.UseVisualStyleBackColor = true;
            this.btnZaustaviAnketu.Click += new System.EventHandler(this.btnZaustaviAnketu_Click);
            // 
            // FormPogledajRezultate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 615);
            this.Controls.Add(this.btnZaustaviAnketu);
            this.Controls.Add(this.dgvRezultatiAnkete);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1085, 662);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1085, 662);
            this.Name = "FormPogledajRezultate";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rezultati ankete";
            this.Load += new System.EventHandler(this.FormPogledajRezultate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRezultatiAnkete)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRezultatiAnkete;
        private System.Windows.Forms.Button btnZaustaviAnketu;
    }
}