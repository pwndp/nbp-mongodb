﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anketa1389.Controls;
using Anketa1389.Entities;

namespace Anketa1389.Forms
{
    public partial class FormPopuniAnketu : Form
    {
        public MongoDBDataLayer client;
        public Anketa anketa;
        public int brojPitanja;
        public int ukupanBrojStrana;
        public int trenutnaStrana;

        public FormPopuniAnketu()
        {
            InitializeComponent();
        }

        public void Inicijalizacija()
        {
            if (anketa == null)
                return;

            lblNazivAnkete.Text = anketa.naziv;
            lblOpisAnkete.Text = anketa.opis;

            pitanjeControl1.roditelj = this;
            pitanjeControl2.roditelj = this;
            pitanjeControl3.roditelj = this;
            btnLevo.Enabled = false;    // Inicijalno ulevo ne moze da se ide
                                        // jer se vec nalazimo na prvoj strani
            trenutnaStrana = 1;

            brojPitanja = anketa.listaPitanja.Count;
            if (brojPitanja <= 3)
            {
                ukupanBrojStrana = 1;
                btnDesno.Enabled = false;
            }
            else
            {
                decimal tri = 3;    // Pomocna promenljiva koja omogucava rad fje Ceiling
                ukupanBrojStrana = (int)Math.Ceiling(brojPitanja / tri);
                btnDesno.Enabled = true;
            }
            PrikaziKontrole();
        }

        private void btnDesno_Click(object sender, EventArgs e)
        {
            if (trenutnaStrana < ukupanBrojStrana)  // Za svaki slucaj
            {
                trenutnaStrana++;
            }

            btnLevo.Enabled = true;

            if (trenutnaStrana >= ukupanBrojStrana)
            {
                btnDesno.Enabled = false;
            }
            PrikaziKontrole();
        }

        private void btnLevo_Click(object sender, EventArgs e)
        {
            if (trenutnaStrana > 1)
            {
                trenutnaStrana--;
            }

            btnDesno.Enabled = true;

            if (trenutnaStrana <= 1)
            {
                btnLevo.Enabled = false;
            }
            PrikaziKontrole();


        }

        private void pitanjeControl1_Load(object sender, EventArgs e)
        {

        }

        private void FormPopuniAnketu_Load(object sender, EventArgs e)
        {
            Inicijalizacija();
        }

        private void PrikaziKontrole()
        {
            PitanjeControl[] nizPC = { pitanjeControl1, pitanjeControl2, pitanjeControl3 };
            for (int i = 0; i < 3; i++)
            {
                int index = 3 * (trenutnaStrana - 1) + i;
                if (index < anketa.listaPitanja.Count)
                {
                    nizPC[i].pitanje = anketa.listaPitanja[index];
                }
                else
                {
                    nizPC[i].pitanje = null;
                }

                    nizPC[i].Prikaz();
            }
        }

        private void btnPosalji_Click(object sender, EventArgs e)
        {
            if (!proveraPodataka())
                return;

            Anketa novaAnketa = new Anketa();
            novaAnketa.naziv = anketa.naziv;
            novaAnketa.opis = anketa.opis;
            novaAnketa.mid = anketa.mid;
            novaAnketa.listaPitanja = anketa.listaPitanja;

            client.DodajPopunjenuAnketu(novaAnketa);

            for (int i = 0; i < anketa.listaPitanja.Count; i++)
            {
                switch (anketa.listaPitanja[i].tip)
                {
                    case tipPitanja.daNe:
                        PitanjeDaNe pom0 = anketa.listaPitanja[i] as PitanjeDaNe;
                        pom0.odgovor = null;
                        break;
                    case tipPitanja.listaBrojeva:
                        PitanjeListaInt pom1 = anketa.listaPitanja[i] as PitanjeListaInt;
                        pom1.odgovor = null;
                        break;
                    case tipPitanja.listaStringova:
                        PitanjeListaStringova pom2 = anketa.listaPitanja[i] as PitanjeListaStringova;
                        pom2.odgovor = null;
                        break;
                    case tipPitanja.slobodanUnosBrojeva:
                        PitanjeSUBrojeva pom3 = anketa.listaPitanja[i] as PitanjeSUBrojeva;
                        pom3.odgovor = null;
                        break;
                    case tipPitanja.tekstualno:
                        PitanjeString pom4 = anketa.listaPitanja[i] as PitanjeString;
                        pom4.odgovor = null;
                        break;
                }
            }

            this.Close();

        }

        private bool proveraPodataka()
        {

            for(int i = 0; i < anketa.listaPitanja.Count(); i++) {
                Pitanje p = anketa.listaPitanja[i];
                if (p.daLiJeObavezno)
                {
                    switch (p.tip)
                    {
                        case tipPitanja.listaStringova:
                            if (String.IsNullOrWhiteSpace((p as PitanjeListaStringova).odgovor))
                            {
                                MessageBox.Show("Pitanje " + (i + 1) + " je obavezno. Molimo Vas popunite ga.");
                                return false;
                            }
                            break;
                        case tipPitanja.listaBrojeva:
                            if (((p as PitanjeListaInt).odgovor)==null)
                            {
                                MessageBox.Show("Pitanje " + (i + 1) + " je obavezno. Molimo Vas popunite ga.");
                                return false;
                            }
                            break;
                        case tipPitanja.tekstualno:
                            if (String.IsNullOrWhiteSpace((p as PitanjeString).odgovor))
                            {
                                MessageBox.Show("Pitanje " + (i + 1) + " je obavezno. Molimo Vas popunite ga.");
                                return false;
                            }
                            break;
                        case tipPitanja.slobodanUnosBrojeva:
                            if (((p as PitanjeSUBrojeva).odgovor) == null)
                            {
                                MessageBox.Show("Pitanje " + (i + 1) + " je obavezno. Molimo Vas popunite ga.");
                                return false;
                            }
                            break;
                        case tipPitanja.daNe:
                            if ( (p as PitanjeDaNe).odgovor == null )
                            {
                                MessageBox.Show("Pitanje " + (i + 1) + " je obavezno. Molimo Vas popunite ga.");
                                return false;
                            }
                            break;
                    }
                }
            }

            return true;
        }
    }
}
