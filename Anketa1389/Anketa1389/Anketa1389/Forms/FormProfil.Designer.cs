﻿namespace Anketa1389.Forms
{
    partial class FormProfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxAnkete = new System.Windows.Forms.GroupBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnKreirajAnketu = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.lblUsername = new System.Windows.Forms.Label();
            this.acSlot3 = new Anketa1389.Controls.AnketaControl();
            this.acSlot2 = new Anketa1389.Controls.AnketaControl();
            this.acSlot1 = new Anketa1389.Controls.AnketaControl();
            this.gbxAnkete.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxAnkete
            // 
            this.gbxAnkete.Controls.Add(this.btnNext);
            this.gbxAnkete.Controls.Add(this.btnPrev);
            this.gbxAnkete.Controls.Add(this.acSlot3);
            this.gbxAnkete.Controls.Add(this.acSlot2);
            this.gbxAnkete.Controls.Add(this.acSlot1);
            this.gbxAnkete.Location = new System.Drawing.Point(29, 97);
            this.gbxAnkete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxAnkete.Name = "gbxAnkete";
            this.gbxAnkete.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxAnkete.Size = new System.Drawing.Size(683, 400);
            this.gbxAnkete.TabIndex = 0;
            this.gbxAnkete.TabStop = false;
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Location = new System.Drawing.Point(619, 178);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(45, 42);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "->";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrev.Location = new System.Drawing.Point(17, 175);
            this.btnPrev.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(45, 42);
            this.btnPrev.TabIndex = 3;
            this.btnPrev.Text = "<-";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnKreirajAnketu
            // 
            this.btnKreirajAnketu.Location = new System.Drawing.Point(576, 36);
            this.btnKreirajAnketu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnKreirajAnketu.Name = "btnKreirajAnketu";
            this.btnKreirajAnketu.Size = new System.Drawing.Size(136, 34);
            this.btnKreirajAnketu.TabIndex = 1;
            this.btnKreirajAnketu.Text = "Kreiraj Anketu";
            this.btnKreirajAnketu.UseVisualStyleBackColor = true;
            this.btnKreirajAnketu.Click += new System.EventHandler(this.btnKreirajAnketu_Click);
            // 
            // btnLogOut
            // 
            this.btnLogOut.Location = new System.Drawing.Point(41, 36);
            this.btnLogOut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(136, 34);
            this.btnLogOut.TabIndex = 0;
            this.btnLogOut.Text = "Odjavi se";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // lblUsername
            // 
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(242, 28);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(255, 48);
            this.lblUsername.TabIndex = 3;
            this.lblUsername.Text = "Username";
            this.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // acSlot3
            // 
            this.acSlot3.Location = new System.Drawing.Point(80, 270);
            this.acSlot3.Margin = new System.Windows.Forms.Padding(5);
            this.acSlot3.Name = "acSlot3";
            this.acSlot3.Size = new System.Drawing.Size(516, 103);
            this.acSlot3.TabIndex = 2;
            // 
            // acSlot2
            // 
            this.acSlot2.Location = new System.Drawing.Point(80, 148);
            this.acSlot2.Margin = new System.Windows.Forms.Padding(5);
            this.acSlot2.Name = "acSlot2";
            this.acSlot2.Size = new System.Drawing.Size(516, 103);
            this.acSlot2.TabIndex = 1;
            // 
            // acSlot1
            // 
            this.acSlot1.Location = new System.Drawing.Point(80, 23);
            this.acSlot1.Margin = new System.Windows.Forms.Padding(5);
            this.acSlot1.Name = "acSlot1";
            this.acSlot1.Size = new System.Drawing.Size(516, 103);
            this.acSlot1.TabIndex = 0;
            // 
            // FormProfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 526);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.btnKreirajAnketu);
            this.Controls.Add(this.gbxAnkete);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(763, 573);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(763, 573);
            this.Name = "FormProfil";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Profil";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormProfil_FormClosing);
            this.gbxAnkete.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxAnkete;
        private Controls.AnketaControl acSlot3;
        private Controls.AnketaControl acSlot2;
        private Controls.AnketaControl acSlot1;
        private System.Windows.Forms.Button btnKreirajAnketu;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
    }
}