﻿using Anketa1389.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anketa1389.Forms
{
    public partial class FormPrijava : Form
    {
        public MongoDBDataLayer client;
        public FormPrijava()
        {
            InitializeComponent();
        }

        private void btnPrijava_Click(object sender, EventArgs e)
        {
            if (!loginDataOkay())
                return;

            var tmpk = new Korisnik { username = tbxUsername.Text, password = tbxPassword.Text };
            var userCheck = client.ProveriPodatke(tmpk);

            if (!userCheck)
            {
                MessageBox.Show("Neispavno korisnicko ime ili lozinka.");
                return;
            }

            Korisnik user = client.VratiKorisnikovePodatke(tmpk);
            FormProfil frm = new FormProfil();
            frm.client = client;
            frm.Korisnik = user;
            this.Visible = false;
            DialogResult dr = frm.ShowDialog();
            if (dr == DialogResult.OK)
            {
                this.Close();
            }
        }

        private bool loginDataOkay()
        {
            if (String.IsNullOrWhiteSpace(tbxUsername.Text))
            {
                MessageBox.Show("Korisnicko ime ne moze biti prazno.");
                return false;
            }

            if (String.IsNullOrWhiteSpace(tbxPassword.Text))
            {
                MessageBox.Show("Lozinka ne moze biti prazna.");
                return false;
            }

            return true;
        }

        private void btnRegistracija_Click(object sender, EventArgs e)
        {
            FormRegistracija frm = new FormRegistracija();
            this.Visible = false;
            frm.client = client;
            frm.ShowDialog();
            this.Visible = true;
        }
    }
}
