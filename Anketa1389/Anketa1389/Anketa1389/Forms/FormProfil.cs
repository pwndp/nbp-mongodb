﻿using Anketa1389.Controls;
using Anketa1389.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anketa1389.Forms
{
    public partial class FormProfil : Form
    {
        public MongoDBDataLayer client;
        private Korisnik _korisnik;
        public Korisnik Korisnik
        {
            get
            {
                return _korisnik;
            }
            set
            {
                _korisnik = value;
                initData();
            }
        }
        int page, maxPage;

        public FormProfil()
        {
            InitializeComponent();
            acSlot1.daLiPrikazujemoStatistiku = true;
            acSlot2.daLiPrikazujemoStatistiku = true;
            acSlot3.daLiPrikazujemoStatistiku = true;
            acSlot1.roditelj = this;
            acSlot2.roditelj = this;
            acSlot3.roditelj = this;

            var dummyUser = new Korisnik() { username = "Dummy username" };
            Korisnik = dummyUser;
        }

        private void initData()
        {
            if (Korisnik == null)
                return;

            lblUsername.Text = Korisnik.username;
            
            page = 0;
            maxPage = (Korisnik.kreiraneAnkete.Count - 1) / 3;
            if (maxPage < 0)
                maxPage = 0;

            btnPrev.Enabled = false;
            if (maxPage == 0)
                btnNext.Enabled = false;
            else
                btnNext.Enabled = true;

            refreshView();
        }

        private void refreshView()
        {
            AnketaControl[] niz = { acSlot1, acSlot2, acSlot3 };
            for (int i = 0; i < 3; i++)
            {
                int index = page * 3 + i;
                niz[i].Anketa = null;
                if (index < Korisnik.kreiraneAnkete.Count)
                    niz[i].Anketa = Korisnik.kreiraneAnkete[index];
                niz[i].Prikaz();
            }
        }

        #region Events

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormProfil_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Da li ste sigurni da zelite da se odjavite ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;
            }
            DialogResult = DialogResult.OK;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            page++;
            if (page > maxPage)
                page = maxPage;
            if (page==maxPage)
            {
                btnNext.Enabled = false;
                if (maxPage != 0)
                    btnPrev.Enabled = true;
                else
                    btnPrev.Enabled = false;
            }
            refreshView();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            page--;
            if (page < 0)
                page = 0;
            if (page == 0)
            {
                btnPrev.Enabled = false;
                if (maxPage != 0)
                    btnNext.Enabled = true;
                else
                    btnNext.Enabled = false;
            }
            refreshView();
        }

        private void btnKreirajAnketu_Click(object sender, EventArgs e)
        {
            FormNapraviAnketu frm = new FormNapraviAnketu();
            frm.Roditelj = this;
            frm.client = client;
            if (frm.ShowDialog() == DialogResult.Yes)
            {
                Korisnik = Korisnik; // Reassign to update maxPage
            }
        }

        #endregion

    }
}
