﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anketa1389.Entities;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace Anketa1389
{
    public class MongoDBDataLayer
    {
        readonly string connectionString = "mongodb://localhost/?safe=true";

        #region Funkcije

        public bool ProveriPostojanjeKorisnika(string username)
        {
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");

            var korisnici = db.GetCollection<Korisnik>("korisnici");
            //korisnici.Drop();
            var res = korisnici.FindAll()?.ToArray().Where((Korisnik k) => k.username == username)?.ToArray();
            bool postoji = false;
            if (res.Length>0)
                postoji = true;
            
            return postoji;
        }

        public bool DodajKorisnika(Korisnik k)
        {
            try {
                var server = MongoServer.Create(connectionString);
                var db = server.GetDatabase("Ankete");
                var korisnici = db.GetCollection<Korisnik>("korisnici");
                korisnici.EnsureIndex(IndexKeys.Ascending("username"), IndexOptions.SetUnique(true));
                korisnici.Insert(k);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AzurirajKorisnika(Korisnik k)
        {
            if (!ProveriPostojanjeKorisnika(k.username))
                return false;

            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");
            var korisnici = db.GetCollection<Korisnik>("korisnici");

            var kveri = Query.EQ("username", k.username);
           // var update = Update.Set("kreiraneAnkete", BsonValue.Create(k.kreiraneAnkete) ); // dokumenti ili kolekcije se ne mogu (iz nekog razloga) postaviti na ovaj nacin
            var update2 = Update.SetWrapped<List<Anketa>>("kreiraneAnkete", k.kreiraneAnkete);
            korisnici.Update(kveri, update2);

            return true;
        }

        public bool ProveriPodatke(Korisnik k)
        {
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");
            var korisnici = db.GetCollection<Korisnik>("korisnici");

            var res = korisnici.FindAll()?.ToArray().Where((Korisnik kor) => kor.username == k.username && kor.password==k.password)?.ToArray();
            bool postoji = false;
            if (res.Length > 0)
                postoji = true;

            return postoji;
        }

        public Korisnik VratiKorisnikovePodatke(Korisnik k)
        {
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");
            var korisnici = db.GetCollection<Korisnik>("korisnici");

            var res = korisnici.FindAll()?.ToArray().Where((Korisnik kor) => kor.username == k.username && kor.password == k.password)?.ToArray();
            Korisnik tmp = null;
            if (res.Length > 0)
                tmp = res[0];

            return tmp;
        }

        public void DodajAnketu(Anketa a, Korisnik kor)
        {
            Korisnik tmpkor = VratiKorisnikovePodatke(kor);
            a.mid = NaredniIDAnkete();
            tmpkor.kreiraneAnkete.Add(a);
            AzurirajKorisnika(tmpkor);
            dodajAnketuUListuAnketa(a);
        }

        public List<Anketa> VratiAnkete()
        {
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");
            var ankete = db.GetCollection<Anketa>("ankete");

            List<Anketa> anks = new List<Anketa>();

            if (ankete.Count() < 1)
                return anks;

            anks = ankete.FindAll().ToList();
            return anks;
        }

        public List<Anketa> VratiAnketeFiltrirano(string oznake)
        {
            string[] tmptags = (oznake+" ").Split(' '); // Za slucaj da je samo jedna rec, split bi bacio gresku
            string[] tags = tmptags.Where(x => !String.IsNullOrWhiteSpace(x) )?.ToArray();
            List<Anketa> ank = VratiAnkete();

            if (ank != null)
            {
                ank = ank.Select(x =>
                {
                    foreach (string tag in tags)
                    {
                        if (x.tagovi.Contains(tag))
                            return x;
                    }
                    return null;
                }).Where(x => x != null).ToList();
            }
            return ank;
        }

        public bool DodajPopunjenuAnketu(Anketa a)
        {
            try
            {
                var server = MongoServer.Create(connectionString);
                var db = server.GetDatabase("Ankete");
                var anketepop = db.GetCollection<Anketa>("ankete-popunjene");
                anketepop.Insert(a);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public List<Anketa> VratiPopunjeneAnkete(Anketa a)
        {
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");
            var anketepop = db.GetCollection<Anketa>("ankete-popunjene");
            if (anketepop.Count() < 1)
                return new List<Anketa>();

            var lst = anketepop.FindAll().Where(x => x.mid == a.mid).ToList();
            return lst;
        }

        public bool StopirajAnketu(Anketa a, Korisnik k)
        {
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");

            // Prvo izmeni stanje ankete na neaktivno
            for (int i = 0; i < k.kreiraneAnkete.Count; i++)
            {
                if (k.kreiraneAnkete[i].naziv == a.naziv && k.kreiraneAnkete[i].opis == a.opis)
                {
                    k.kreiraneAnkete[i].daLiJeAktivna = false;
                    break;
                }
            }

            AzurirajKorisnika(k);
            var ankete = db.GetCollection<Anketa>("ankete");
            // Ovim stopiramo anketu, odnosnu uklanjamo je iz liste dostupnih anketa.
            // Ipak, popunjene ankete su ostale i one se uklanjaju pozivom fcije UkloniAnketu(anketa);
            var kveri = Query.EQ("mid", BsonValue.Create(a.mid));
            ankete.Remove(kveri);
            
            return true;
        }
        
        public bool UkloniAnketu(Anketa a)
        {
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");
            
            // Anketa je stopirana, tako da nam preostaje samo da obrisemo listu svih popunjenih anketa
            var anketepop = db.GetCollection<Anketa>("ankete-popunjene");

            var kveri = Query.EQ("mid", BsonValue.Create(a.mid));
            anketepop.Remove(kveri);

            return true;
        }
        
        #endregion

        #region Privatne fcije

        private bool dodajAnketuUListuAnketa(Anketa a)
        {
            try
            {
                var server = MongoServer.Create(connectionString);
                var db = server.GetDatabase("Ankete");
                var ankete = db.GetCollection<Anketa>("ankete");
                ankete.EnsureIndex(IndexKeys.Ascending("mid"), IndexOptions.SetUnique(true));
                ankete.Insert(a);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private int NaredniIDAnkete()
        {
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("Ankete");
            var ankete = db.GetCollection<Anketa>("ankete");
            int br = 0; // Inicijalni id koji vracamo

            if (ankete.Count() > 0)
            {
                foreach (Anketa a in ankete.FindAll().ToArray())
                {
                    if (a.mid > br)
                        br = a.mid;
                }
            }

            // Sada uvecaj broj, jer zelimo naredni
            br++;
            return br;
        }
        
        #endregion

    }
}

/*
Ispitivanje da li postoji korisnik prilikom registracije (done)
Registracija korisnika (done)
Ispitivanje da li su ispravni korisnicki podaci prilikom logovanja (done)
Logovanje (i pribavljanje korisnikovih podataka) (done)
Dodavanje ankete (done)
Brisanje ankete (done) 
Pribavljanje anketa (za glavni ekran) (done)
Pribavljanje anketa (na osnovu tagova, za glavni ekran) (done)
Dodavanje popunjene ankete (done)
Pribavljanje podataka o popunjenim anketama (done)
Brisanje podataka o anketi kada je korisnik ukloni iz svoje liste (done)
*/
