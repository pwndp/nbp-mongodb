﻿namespace Anketa1389
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxAnkete = new System.Windows.Forms.GroupBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.acSlot3 = new Anketa1389.Controls.AnketaControl();
            this.acSlot2 = new Anketa1389.Controls.AnketaControl();
            this.acSlot1 = new Anketa1389.Controls.AnketaControl();
            this.btnPretraga = new System.Windows.Forms.Button();
            this.btnPrijava = new System.Windows.Forms.Button();
            this.tbxOznake = new System.Windows.Forms.TextBox();
            this.lblOznake = new System.Windows.Forms.Label();
            this.gbxAnkete.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxAnkete
            // 
            this.gbxAnkete.Controls.Add(this.btnNext);
            this.gbxAnkete.Controls.Add(this.btnPrev);
            this.gbxAnkete.Controls.Add(this.acSlot3);
            this.gbxAnkete.Controls.Add(this.acSlot2);
            this.gbxAnkete.Controls.Add(this.acSlot1);
            this.gbxAnkete.Location = new System.Drawing.Point(17, 68);
            this.gbxAnkete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxAnkete.Name = "gbxAnkete";
            this.gbxAnkete.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxAnkete.Size = new System.Drawing.Size(649, 366);
            this.gbxAnkete.TabIndex = 3;
            this.gbxAnkete.TabStop = false;
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Location = new System.Drawing.Point(597, 169);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(45, 37);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "->";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrev.Location = new System.Drawing.Point(23, 169);
            this.btnPrev.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(45, 37);
            this.btnPrev.TabIndex = 3;
            this.btnPrev.Text = "<-";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // acSlot3
            // 
            this.acSlot3.Location = new System.Drawing.Point(76, 245);
            this.acSlot3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.acSlot3.Name = "acSlot3";
            this.acSlot3.Size = new System.Drawing.Size(516, 103);
            this.acSlot3.TabIndex = 2;
            // 
            // acSlot2
            // 
            this.acSlot2.Location = new System.Drawing.Point(76, 134);
            this.acSlot2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.acSlot2.Name = "acSlot2";
            this.acSlot2.Size = new System.Drawing.Size(516, 103);
            this.acSlot2.TabIndex = 1;
            // 
            // acSlot1
            // 
            this.acSlot1.Location = new System.Drawing.Point(76, 23);
            this.acSlot1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.acSlot1.Name = "acSlot1";
            this.acSlot1.Size = new System.Drawing.Size(516, 103);
            this.acSlot1.TabIndex = 0;
            // 
            // btnPretraga
            // 
            this.btnPretraga.Location = new System.Drawing.Point(559, 18);
            this.btnPretraga.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPretraga.Name = "btnPretraga";
            this.btnPretraga.Size = new System.Drawing.Size(108, 32);
            this.btnPretraga.TabIndex = 2;
            this.btnPretraga.Text = "Pretrazi";
            this.btnPretraga.UseVisualStyleBackColor = true;
            this.btnPretraga.Click += new System.EventHandler(this.btnPretraga_Click);
            // 
            // btnPrijava
            // 
            this.btnPrijava.Location = new System.Drawing.Point(36, 15);
            this.btnPrijava.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPrijava.Name = "btnPrijava";
            this.btnPrijava.Size = new System.Drawing.Size(129, 41);
            this.btnPrijava.TabIndex = 0;
            this.btnPrijava.Text = "Prijavi se";
            this.btnPrijava.UseVisualStyleBackColor = true;
            this.btnPrijava.Click += new System.EventHandler(this.btnPrijava_Click);
            // 
            // tbxOznake
            // 
            this.tbxOznake.Location = new System.Drawing.Point(328, 23);
            this.tbxOznake.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxOznake.MaxLength = 127;
            this.tbxOznake.Name = "tbxOznake";
            this.tbxOznake.Size = new System.Drawing.Size(219, 22);
            this.tbxOznake.TabIndex = 1;
            this.tbxOznake.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxOznake_KeyPress);
            // 
            // lblOznake
            // 
            this.lblOznake.Location = new System.Drawing.Point(173, 18);
            this.lblOznake.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOznake.Name = "lblOznake";
            this.lblOznake.Size = new System.Drawing.Size(147, 46);
            this.lblOznake.TabIndex = 7;
            this.lblOznake.Text = "Oznake \r\n(odvojene razmakom)";
            this.lblOznake.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 436);
            this.Controls.Add(this.lblOznake);
            this.Controls.Add(this.tbxOznake);
            this.Controls.Add(this.gbxAnkete);
            this.Controls.Add(this.btnPretraga);
            this.Controls.Add(this.btnPrijava);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(703, 483);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(703, 483);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Anketa 1389";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.gbxAnkete.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxAnkete;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private Controls.AnketaControl acSlot3;
        private Controls.AnketaControl acSlot2;
        private Controls.AnketaControl acSlot1;
        private System.Windows.Forms.Button btnPretraga;
        private System.Windows.Forms.Button btnPrijava;
        private System.Windows.Forms.TextBox tbxOznake;
        private System.Windows.Forms.Label lblOznake;
    }
}

