﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Anketa1389.Entities
{
 
    public class PitanjeString : Pitanje
    {
        
        public string odgovor;

        public PitanjeString()
        {
            tip = tipPitanja.tekstualno;
            odgovor = "";
        }
    }
}
