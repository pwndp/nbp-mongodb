﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Anketa1389.Entities
{
    [Serializable]
    public class Anketa
    {
        public ObjectId Id;
        public int mid;
        public string naziv;
        public string opis;
        public List<Pitanje> listaPitanja;
        public bool daLiJeAktivna;
        public int brojUcesnika;
        public string tagovi; //Split, recimo, ","

        public Anketa()
        {
            brojUcesnika = 0;
            naziv = "";
            opis = "";
            daLiJeAktivna = true;
            tagovi = "";
            listaPitanja = new List<Pitanje>();
        }

        public void DodajPitanje(Pitanje pit)
        {
            listaPitanja.Add(pit);
        }
    }
}
