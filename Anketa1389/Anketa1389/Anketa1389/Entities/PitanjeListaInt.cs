﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Anketa1389.Entities
{

    public class PitanjeListaInt : Pitanje
    {
        
        public int? odgovor;
        
        public List<int> ponudjeniOdgovoriInt;

        public PitanjeListaInt(List<int> odg)
        {
            ponudjeniOdgovoriInt = odg;
            tip = tipPitanja.listaBrojeva;
        }

    }
}
