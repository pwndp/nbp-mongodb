﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Anketa1389.Entities
{

    public class PitanjeDaNe : Pitanje
    {
        
        public string[] ponudjeniOdgovori = {"Da", "Ne"};
        
        public bool? odgovor;

        public PitanjeDaNe()
        {
            tip = tipPitanja.daNe;
        }
    }
}
