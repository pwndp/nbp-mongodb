﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Anketa1389.Entities
{

    public class PitanjeListaStringova : Pitanje
    {
        
        public string odgovor;
        
        public List<string> ponudjeniOdgovoriString;

        public PitanjeListaStringova(List<string> odg)
        {
            ponudjeniOdgovoriString = odg;
            tip = tipPitanja.listaStringova;
        }
    }
}
