﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Anketa1389.Entities
{
    public enum tipPitanja
    {
        tekstualno =1,
        listaStringova,
        listaBrojeva,
        slobodanUnosBrojeva,
        daNe
    };

    [Serializable]
    [BsonKnownTypes(typeof(PitanjeListaInt), typeof(PitanjeListaStringova), typeof(PitanjeString), typeof(PitanjeSUBrojeva), typeof(PitanjeDaNe))]
    public class Pitanje
    {
        
        public ObjectId Id;
        public string tekstPitanja="";
        public tipPitanja tip;
        public bool daLiJeObavezno=false;
    }
}
