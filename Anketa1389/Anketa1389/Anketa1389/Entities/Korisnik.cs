﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anketa1389.Entities
{
    public class Korisnik
    {
        public ObjectId Id;
        public string username;
        public string password;
        public List<Anketa> kreiraneAnkete = new List<Anketa>(); 

        public void DodajAnketu(Anketa an)
        {
            kreiraneAnkete.Add(an);
        }

    }
}
